- name: MRARR
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: MRARR is the measurement of Merge Requests from customers, multiplied with the revenue of the account (ARR) from that customer.
    This measures how active our biggest customers are contributing to GitLab. We believe the higher this number the better we'll retain these customers and improve product fit for large enterprises.
    The unit of MRARR is MR Dollars (MR$). MR Dollars is different than the normal Dollars which is used for ARR.
  target: Greater than 20M MR Dollars per month
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
    - Efforts in FY22-Q2 to raise to 30M MR dollars
    - $7M in the pipeline from April and $10M in pre-April stale pipeline
    - Short term challenge with data lag which is affecting our visibility
    - Collaboration with TAMs and Community team have received reduced attention due to us prioritizing a key opportunity.
  sisense_data:
    chart: 10081556
    dashboard: 765640
    embed: v2
    shared_dashboard: 36531745-61b0-4fce-aa45-7a5af97762b5
  urls:
    - https://gitlab.com/groups/gitlab-com/-/epics/1225
    - https://app.periscopedata.com/app/gitlab/765640
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/9456
- name: Percent of MRs from Community
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: This is the ratio of community contributions to the number of merged product MRs. As we grow as a company we want to make sure the community scales with the company.
  target: TBD
  org: Quality Department
  is_key: true
  health:
    level: 0
    reasons:
      - Need to define target
  sisense_data:
    chart: 11016270
    dashboard: 824124
    shared_dashboard: 803b4ecb-7152-42a1-ae93-b01963edad62
    embed: v2    
- name: GitLab project average successful merge request pipeline duration
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average successful duration the GitLab project merge request pipelines. Key building block to improve
    our cycle time, and effiency.
  target: Below 45 minutes
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
      - Corrections helped reduce to 55mins in April
      - Switch runner machines to n2d-standard-2 helped performance for CPU bound builds
      - May's increase is based on less than 1% of pipelines available for measurement due to data lag. The data is not representative of a normal monthly distribution.
  urls:
    - https://gitlab.com/groups/gitlab-org/-/epics/1853
    - https://gitlab.com/groups/gitlab-org/-/epics/5987
    - https://gitlab.com/gitlab-org/gitlab/-/issues/322726
  sisense_data:
    chart: 6782964
    dashboard: 516343
    embed: v2
- name: GitLab project master pipeline success rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of the GitLab project master branch success rate. A key indicator to the stability of our releases.
  target: Above 95%
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
      - Increased slightly in April to 89%
      - Recent drop is due to increase in flaky spec frequency and frequent segmentation fault. Segmentation faults have improved but not fully resolved, reducing flaky spec frequency is a Q2 team KR.
  urls:
    - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/195#known-issues-improvements
    - https://gitlab.com/gitlab-org/gitlab/-/issues/326348
    - https://gitlab.com/gitlab-org/gitlab/-/issues/330998
  sisense_data:
    chart: 8573283
    dashboard: 516343
    embed: v2
- name: Review App deployment success rate for GitLab
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability of our test tooling to enable engineering efficiency.
  target: Above 99%
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
      - Increased to 97% in April
      - Increased utilization for QA automated tests increased resource demands on review apps cluster leading to scalability challenges and reduction for May. We will collaborate with Distribution to stabilize the usage of GitLab Charts.
  urls:
    - https://gitlab.com/groups/gitlab-org/-/epics/605
    - https://gitlab.com/groups/gitlab-org/-/epics/606
    - https://gitlab.com/groups/gitlab-org/-/epics/6024
  sisense_data:
    chart: 6721558
    dashboard: 516343
    embed: v2
- name: GitLab project merge request pipeline average time to failure
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the feedback loop efficiency to team members when there is a failure that needs action. The Quality Department <a href="https://youtu.be/6dK5j8ADa3U?t=573">recently discussed</a> the desire to measure the average time to first failure. We want to run the tests that are likely to fail first and shorten the feedback cycle to our R&D teams. The team is working on <a href="https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11793"> adjusting this metric</a> to fulfill this intention.
  target: Below 30 minutes
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
      - Decreased to 51mins in April
      - May's increase is based on less than 1% of pipelines available for measurement due to data lag. The data is not representative of a normal monthly distribution.
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10116
  sisense_data:
    chart: 11047190
    dashboard: 516343
    embed: v2
- name: Average Age of Open S1 Bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average age calculates the average time a bug has been open as of the reported day. Number of bugs is shown to provide context to Average Age. This performance indicator is scoped to S1 open bug issues for all is_part_of_product projects on a daily aggregation for the past 1 year.
  target: Below 150 days
  org: Quality Department
  is_key: true
  health:
    level: 2
    reasons:
      - Set target to below 150 days, a decrease of 50% from current.
      - Number of S1s are low and we think we can make an impact here in the coming releases.
  sisense_data:
    dashboard: 856737
    chart: 11589354
    shared_dashboard: ae618b53-194d-40ed-a3ca-7f0d4ca9a492
    embed: v2
- name: Average Age of Open S2 Bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average age calculates the average time a bug has been open as of the reported day. Number of bugs is shown to provide context to Average Age. This performance indicator is scoped to S2 open bug issues for all is_part_of_product projects on a daily aggregation for the past 1 year.
  org: Quality Department
  target: Below 300 days
  is_key: true
  health:
    level: 2
    reasons:
    - Set target to below 300 days, a decrease of 25% from current.
    - Number of S2s are increasing, we need to revitialize the bug refinement process to hone in on S2s
    - 20% of open S2s have been open for more than 2 years, we will focus on this area in the next refinement.
  sisense_data:
    dashboard: 856747
    chart: 11589527
    shared_dashboard: 0ca65051-9c59-4cac-9d2b-e2077296d2a7
    embed: v2
- name: Software Engineer in Test Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Software Engineers in Test against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 45 Software Engineers in Test
  org: Quality Department
  is_key: true
  health:
    level: 1
    reasons:
    - Currently at 37% of target
    - We hired 2 SETs for Pipeline Authoring group and Import group
    - We have 2 more positions activated as part of staffing requests
    - In addition to staffing requests we are ensuring that we put our SETs in the highly needed areas
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/11485
  sisense_data:
    chart: 9668867
    dashboard: 516343
    embed: v2
- name: Quality Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor by function and department so managers can make tradeoffs
    and hire in an expensive region when they really need specific talent unavailable
    elsewhere, and offset it with great people who happen to be in low cost areas.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Recent upward trend is due to geo location factor revision. No new hires during uptick.
      - We are focused on location factors of our new hires and vacancies and targetting efficient locations globally.
      - We are considering adjusting to 0.66 to reflect the division KPI.
  sisense_data:
    chart: 7045895
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"
- name: Quality Budget Plan vs Actuals
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-budget-vs-plan"
  definition: We need to spend our investors' money wisely. We also need to run a
    responsible business to be successful, and to one day go on the public market.
  target: Unknown until FY22 planning process
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - Data now lives in adaptive for FY22, not in data warehouse. This is a Q2 priority for data team
  urls:
  - https://app.periscopedata.com/app/gitlab/633240/Quality-Non-Headcount-BvAs
- name: Quality Handbook MR Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-handbook-merge-request-rate"
  definition: The handbook is essential to working remote successfully, to keeping
    up our transparency, and to recruiting successfully. Our processes are constantly
    evolving and we need a way to make sure the handbook is being updated at a regular
    cadence. This data is retrieved by querying the API with a python script for merge
    requests that have files matching <code>/source/handbook/engineering/quality/**</code>over
    time.
  target: Above 1 MR per person per month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Under target, but showing steady increasing trend.
      - Modified KPI to be a rate based metric for department team size.
  sisense_data:
    chart: 10586749
    dashboard: 621059
    shared_dashboard: 3f0e7d87-fc13-44c3-bbba-1a9feb48c3bd
    embed: v2
- name: Quality Hiring Actual vs Plan
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-hiring-actual-vs-plan"
  definition: Employees are in the division "Engineering" and department is "Quality".
  target: 24 by November 1, 2019
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
    - Engineering is on plan. But we are lending some of our recruiters to sales for
      this quarter. And we just put in place a new "one star minimum" rule that might
      decrease offer volume.
  sisense_data:
    chart: 8610186
    dashboard: 516343
    embed: v2
  urls:
  - "/handbook/hiring/charts/quality-department/"
- name: Quality MR Rate
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-mr-rate"
  definition: Quality Department MR rate a key indicator showing how many changes the Quality Department implements directly in the GitLab product.
    It is important because it shows our iterative productivity based on the average MR merged per team member.
    We currently count all members of the Quality Department (Director, EMs, ICs) in the denominator because this is a team effort.
    The full definition of MR Rate is linked in the url section.
  target: Above 10 MRs per Month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Upgraded to attention since we hit our target of 10 in July 2020
    - Working on increasing Quality codebase maintainers and champion smaller iterations
  urls:
    - "/handbook/engineering/metrics/#merge-request-rate"
    - https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/534
  sisense_data:
    chart: 8467527
    dashboard: 654023
    shared_dashboard: 4278f770-7709-4a5e-89f7-812a319c2fbb
    embed: v2
- name: Quality New Hire Average Location Factor
  base_path: "/handbook/engineering/quality/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-division-new-hire-average-location-factor"
  definition: We remain efficient financially if we are hiring globally, working asynchronously,
    and hiring great people in low-cost regions where we pay market rates. We track
    an average location factor for team members hired within the past 3 months so
    hiring managers can make tradeoffs and hire in an expensive region when they really
    need specific talent unavailable elsewhere, and offset it with great people who
    happen to be in more efficient location factor areas with another hire. The historical average location factor represents the average location factor for only new hires in the last three months, excluding internal hires and promotions. The calculation for the three-month rolling average location factor is the location factor of all new hires in the last three months divided by the number of new hires in the last three months for a given hire month. The data source is BambooHR data.
  target: Below 0.58
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
    - We've fluxuated above and below the target line recently, which for a small
      department is not worrisome
  sisense_data:
    chart: 9389208
    dashboard: 719540
- name: Quality Team Member Retention
  base_path: "/handbook/engineering/infrastructure/performance-indicators/"
  parent: "/handbook/engineering/performance-indicators/#engineering-team-member-retention"
  definition: We need to be able to retain talented team members. Retention measures our ability to keep them sticking around at GitLab. Team Member Retention = (1-(Number of Team Members leaving GitLab/Average of the 12 month Total Team Member Headcount)) x 100. GitLab measures team member retention over a rolling 12 month period.
  target: at or above 84%
  org: Quality Department
  is_key: false
  public: false
  health:
    level: 3
    reasons:
    - above target
  urls:
    - "https://app.periscopedata.com/app/gitlab/862331/Quality-Department-Retention" 
- name: Average Age of Open S1 Bugs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average age calculates the average time a bug has been open as of the reported day. Number of bugs is shown to provide context to Average Age. This performance indicator is scoped to S1 open bug issues for all is_part_of_product projects on a daily aggregation for the past 1 year.
  org: Quality Department
  is_key: true
  health:
    level: 0
    reasons:
      - Age is a new metric and being viewed in relation to Mean time to close and untriaged bugs. Target and health will be updated when the performance indicator matures.
  sisense_data:
    dashboard: 856737
    chart: 11589354
    shared_dashboard: ae618b53-194d-40ed-a3ca-7f0d4ca9a492
    embed: v2
- name: Open Bugs with Severity
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Percent known to unknown severity on open bugs.
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
      - We noticed a trend lower
      - More staffing needs to be put towards issue hygine in satellite projects (Gitaly, Runner, Workhorse and etc)
      - We look to improve the representation of this metric and considering moving this to a supplemental chart in Average Age of Open Bugs KPI
  sisense_data:
    dashboard: 576726
    chart: 9987812
    embed: v2
- name: Community Contribution Mean Time to Merge
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Average days from creation to merge for Community contribution MRs by month
  target: Below 10 days
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - This metric is new and we have an intial target of 10 days
  sisense_data:
    chart: 9555034
    dashboard: 729542
    embed: v2
- name: Community MR Coaches per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: The number of MR Coaches defined by team.yml role
  target: Above 50 coaches per month
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Showing growing trend of MR coaches.
      - We are currently working on defining MR coach specialties across Engineering job specialties (Development, Quality, UX)
  sisense_data:
    chart: 9721107
    dashboard: 729542
    embed: v2
  urls:
    - https://gitlab.com/gitlab-com/www-gitlab-com/-/issues/10519
- name: Unique Community Contributors per Month
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Distribution of unique authors for all merged Community contribution MRs by month.
  target: Above 150 contributors per month
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
      - This metric is new and we are working with Community team to identify target
  sisense_data:
    chart: 9522755
    dashboard: 729542
    embed: v2
- name: Percent of Feature Community Contribution MRs
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Percentage of merged `~"Community contribution"` MRs that are labelled `~feature`
  target: Above 30%
  org: Quality Department
  is_key: false
  health:
    level: 3
    reasons:
      - Made progress of hitting goal of 30% by improving Community Contribution automation and improve issue hygiene and refinement for Community Contributors
  sisense_data:
    chart: 9640193
    dashboard: 729542
    embed: v2
- name: GitLab project average pipeline cost per merge request
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure the total pipeline cost per merge request to measure engineering efficiency. This
    is calculated by taking the total cost of all pipelines divided by the number of merge requests.
  target: Below 7.50
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
      - Cost decrease was due to duration improvements and shift to n2d-standard-2 runners.
  sisense_data:
    chart: 11689663
    dashboard: 862651
    embed: v2
    shared_dashboard: a6ec2792-d044-411d-aca7-f4496543a461
  urls:
    - https://gitlab.com/gitlab-org/gitlab/-/issues/322726
- name: Engineering Productivity Engineer Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Engineering Productivity Engineers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 16 Engineering Productivity Engineers
  org: Quality Department
  is_key: false
  health:
    level: 1
    reasons:
    - Currently at 25% of target
  sisense_data:
    chart: 9669589
    dashboard: 516343
    embed: v2
- name: Quality Engineering Manager Gearing Ratio
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Amount of Quality Engineering Managers against the targeted <a href="https://about.gitlab.com/handbook/engineering/quality/#staffing-planning">gearing ratio</a>
  target: At 7 Quality Engineering Managers
  org: Quality Department
  is_key: false
  health:
    level: 2
    reasons:
    - Currently at 57% of target
  sisense_data:
    chart: 9669154
    dashboard: 516343
    embed: v2
- name: Average duration of end-to-end test suite execution on CE/EE master branch
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the average duration of our full QA/end-to-end test suite in
    the <code>master</code> branch to accelerate cycle time of merge requests, and
    continuous deployments.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven't started measuring it yet.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/198
- name: Ratio of quarantine vs total end-to-end tests in CE/EE master branch
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measures the stability and effectiveness of our QA/end-to-end tests
    running in the <code>master</code> branch.
  target: TBD
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven’t started measuring it yet.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/199
- name: New issue first triage SLO
  base_path: "/handbook/engineering/quality/performance-indicators/"
  definition: Measure our speed to triage new issues. We currently have ~400 new issues
    every week in CE/EE. We need to go through all of them and identify valid issues
    and high severity bugs.
  target: Below 5 days
  org: Quality Department
  is_key: false
  health:
    level: 0
    reasons:
    - We haven’t started measuring it yet. We have made progress on fanning out first
      triage to Engineers in the Quality Department.
    - Define an automated mechanism to collect data in Periscope.
    - Define threshold.
    - Fan out triaging to all of Engineering and not just the Quality Department.
  urls:
  - https://gitlab.com/gitlab-org/quality/team-tasks/issues/136
  - https://gitlab.com/groups/gitlab-data/-/epics/50
