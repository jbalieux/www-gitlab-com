---
layout: handbook-page-toc
title: "Entity Level Controls"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## What are ELCs?

ELCs, or Entity Level contorls are a subset of the [SOX internal contol set](https://about.gitlab.com/handbook/finance/sox-internal-controls/). The objectives of ELCs are to mitigate risk threatening the organization as a whole and to provide assurance that organizational objectives are achieved.

### What are GitLab's ELCs?

Beginning in FY22 Q1 GitLab's ELCs for SOX compliance are:


|Sl.no #|Process|Control Mapping
|:---|:---|:---|
|1|Integrity and ethical values|[C.01](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/) and [C.02](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)
|2|Organisational structure|[C.03](https://docs.google.com/document/d/166XksiBM28zzAtsEpkHsFNLMAmz_lcFTxNPfg6WM204/edit?usp=sharing) and [C.04](/handbook/finance/authorization-matrix/)
|3|Board of Directors & Audit Committee|[C.05](/handbook/board-meetings/), [C.06](/handbook/board-meetings/#audit-committee-charter) and [C.07](/handbook/board-meetings/)
|4|Entity-wide objectives|[C.08](https://docs.google.com/document/d/1HALK81OatVCO7tRIj9YzDIaqQqmpCiomT6sT1fHrIIY/edit?usp=sharing)
|5|Risk Assessment|[C.09](/handbook/board-meetings/), C.10, [C.11](/handbook/finance/accounting/), [C.12](/handbook/tax/) and [C.16](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/)
|6|Monitoring|[C.13](/handbook/board-meetings/), [C.14](/handbook/engineering/security/#sts=Information%20Security%20Policies ) and [C.15](/handbook/engineering/development/enablement/geo/)
