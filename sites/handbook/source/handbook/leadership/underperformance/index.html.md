---
layout: handbook-page-toc
title: "Underperformance"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

We want team members to be successful and should offer every opportunity for them to work effectively.
Important deliverables or being understaffed are not good reasons to keep team members who are underperforming.
We owe it to the team to maintain a high standard of performance amongst all team members.
In addition, we also want our team members to be successful, and to recognize that they may be more successful at another company.

In all cases, we want a manager who asks the question "Is this the best person I could hire today?" to respond with a "Yes".

## Training Video

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/nRJHvzXwXBU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>


## Managing Underperformance Handbook Learning Session

Managing and identifying underperformance can be one of the most difficult responsibilities of a manager, but its also one of the most important.  As a people leader, you want to address underperformance in a timely and structured way to be fair and transparent with the team member. During a [CEO Handbook Learning Session](/handbook/people-group/learning-and-development/learning-initiatives/#ceo-handbook-learning-sessions), GitLab CEO, Sid, and the Learning and Development team discuss what underperformance is, how managing it aligns to our values, the impact it can have on teams.  You will also be introduced to the signs to look for to identify underperformance.  We also cover tips team members and managers can implement along the way. 


<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/-mLpytnQtlY" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

> If someone is still not performing at an adequate level after going through the underperformance process, recognize that it's not only better for the company for the team member to move on, it's frequently also better for the team member.  Making a decision to move the team member out of the role allows them to find a role where they can be successful, whether in or outside of GitLab.  The longer you wait, the harder it will be to make a change. If it's not going to work out, take action as soon as possible. - GitLab co-founder and CEO [Sid Sijbrandij](/about.gitlab.com/handbook/ceo/)

### Managing Performance at GitLab

GitLab strives to treat all team members with dignity and respect at all times. You can review GitLab's [Team Member Relation Philosophy](/handbook/people-group/team-member-relations-philosophy/) which outlines how we treat all team members and provides guidelines for how we address issues within GitLab.  The kindest thing we can do for our team members is to address any issue early, and to provide a supportive environment where they can work on the issues and improve their performance.  

Managing a team member who is underperforming is one of the more challenging aspects of the manager role.  Though challenging it is expected that all leaders manage performance timely and provide team members with clear feedback and guidance.  There are many reasons why a leader may not actively manage an underperforming team member.  Below are a few reasons why managers sometimes avoid the difficult performance conversations:  

* It is hard, uncomfortable and the leader fears having the hard conversations
* Favoritism
* Unwillingness to admit you made a mistake in the hiring process
* Believes the performance issue will resolve itself over time

For managers it is important that they address performance issues with team members early, failure to act early can greatly affect the performance of the team as a whole.  Effective teams require cohesion, collaboration and actively engaging and performing team members to be successful in reaching their goals.   Without this cohesiveness there is a risk to the overall team and GitLab's success.

GitLab will use the **skill-based** and **will-based** performance model to identify the root cause of the team members' underperformance and with that determine the suitable remediation plan.  The remediation plan will differ based on the type of performance issue. 

The skills-based and will-based model differs from our [Talent Assessment model](https://gitlab.com/gl-talent-acquisition/operations/-/issues/959#note_566742220) outlined in the handbook.   In determining the root cause of an underperformance issue we will look to determine if the team member has the required skills needed to be successful in the role, or if the team member lacks the will (behavior) required to be successful.   Though they are separate, managers should take into consideration skill- and will-based issues when performing a Talent Assessment for the team member. 

The team member relations specialist is here to assist you in identifying the cause of the performance issue and can walk you through the performance management process that applies best to the situation.  Below are definitions of skills-based and will-based concerns as well as the typical remediation plans.  As with all situations, there may be unique circumstances which require unique remediation plans.  

### Skills-based performance 

Team members may have the motivation to be successful but performance is impacted due to the following reasons:

- Team member lacks the technical capabilities to be successful in current role
- Team member is unable to prioritize work effectively
- Team member lacks the understanding of how to complete tasks required for role
- Team member exhibits difficulties working with other team members and/or communicating effectively

Managers should review the skill-based performance issues and determine if the team member requires additional coaching, training, additional tools/resources, clearer instructions, guidance or assigning the team member a mentor or buddy.  

### Will-based performance

Team members who have the skills necessary to succeed in their current role but they lack the will to perform.   These may be impacted due to the following reasons:  

- Team member is unmotivated or not fully engaged
- Team member is unprofessional with their fellow team members
- Team member exhibits undersirable behavior that impedes their success
- Team member exhibits undersirable behavior that impedes the success of another Team member, Team or GitLab as a whole

If a manager or any team member witnesses or is made aware of any team member behavior that violates our [Code of Conduct](/handbook/legal/gitlab-code-of-business-conduct-and-ethics/) they should immediately reach out to our [Team Member Relations Specialist](/handbook/people-group/#team-member-relations-specialist).  

## Warning Signs/Patterns of Underperformance

How do you know if a team member isn't reaching their goals? Below are some common warning signs and patterns of underperformance as it relates to not meeting the expectations of the role. 

1. Lack of [results](https://about.gitlab.com/handbook/values/#results)
1. Response time 
1. Unclear status of work
1. Frustration of the team member
1. Disagreements or confusion about the goals
1. Frustration from other team members
1. Other team members start working around them
1. Missed goals and timelines

These are all signs of a performance issue, however are these performance issues skill-based or will-based?  Knowing the root cause of the performance issue enables the manager and team member to have open and transparent conversations to implement a plan to improve performance.  

## Act Early

Managers should address performance concerns early and not wait until the performance problems have become unrecoverable.  [Feedback](/handbook/people-group/guidance-on-feedback/#importance-of-feedback) should be given early, it should be specific and it must be documented. 

### Team Member: Discuss Circumstances Immediately With Your Manager

Tell your manager immediately if there are circumstances in your life that cause you to be less effective.
It isn't required to give details if you prefer not to.
Tell your manager when it started, to what extent it hinders your work, and what your forecast is for your circumstances to improve.
When you delay this conversation until your manager identifies underperformance you've lost trust that would be helpful during this period.

### Loop in the Team Member Relations Specialist

Looping in the [Team Member Relations Specialist](https://about.gitlab.com/handbook/people-group/) early is essential. The TMR specialist can offer support, guidance, coaching, and partner with Legal to ensure compliance. Please be sure to align with your TMR specialist prior to delivering any formal action plan to team members. Formal action includes Written Performance Letter, Verbal Warning, Written Warning and a Performance Improvement Plan.  Managers will partner with the team member relations group in the creation and delivery of any formal action. 

### Manager: Identify And Take Action As Early As Possible
Taking action sooner allows the action to be less severe and allows more time for coaching to have an effect.
The important thing to remember as a manager is to immediately address signs of underperformance. Make sure to consider current personal circumstances, whether the team member has taken sufficient [paid time off](/handbook/paid-time-off/) in the last 3 to 6 months, review previous performance feedback and any other indications of why there is underperformance or a team member is not meeting expectation for their role.

### Immediately Discuss With The Manager's Manager

Underperformance should be addressed between the team member, the manager, and their manager.  That is because the manager of the manager needs to see that underperformance is identified early and can help advising proportional actions to address it.
Taking early action to address underperformance is an essential manager skill and one of the most important ways to improve results.
Inform your manager immediately when you've identified possible underperformance. This is an excellent way to demonstrate initiative and rapidly improve your team. 
If your manager warns you about possible underperformance before you notify them, you have not practiced [always tell us the bad news promptly](/company/team/structure/#management-group). This diminishes the trust that is needed to resolve the situation.
Managing underperformance is very important because it reinforces acceptable standards of performance.
It is hard because frequently the underperformance is due to a mistake we made in hiring, onboarding, and/or coaching.
It is also hard because it is a hard message to give and receive that someone's performance is not adequate. 

## Frequency and Metrics
 
We expect above 5% of our team members to experience explicit performance management on an annual basis, and we expect greater than 50% to be successful. We track this in meetings like the People Group Key Review and [People Group Metrics](/handbook/people-group/people-group-metrics/#performance-management) to make sure we're managing performance consistently and fairly across all divisions, and in all circumstances. However, we don't enforce any hard thresholds.
 
This results in a lower rate of involuntary attrition than comparable companies, which range from 5 to 15% annually. We justify this based on the up-front effort we put into the hiring process. Divisions have various quantitative and qualitative processes for ensuring new hires can be successful at GitLab. This includes, but is not limited to:
 
* Requiring two "strong yes" votes from the interview team to make an offer
* Requiring that candidates demonstrate all documented must-have criteria for the role
* Requiring that candidates demonstrate a simple majority of all nice-to-have criteria for the role
* Requiring that hiring managers write a "justification" scorecard that outlines the candidate's strengths, outlines their weaknesses, and articulates a plan for making them successfully
* Requiring that hiring managers fill out a retrospection document for all unsuccessful hires

## Options For Remediation 

Remediation options will vary based on if the team members underperformance is skill-based or will-based and local applicable law.  However, as a best practice and to provide a consistent experience for all team members the following remediation process is recommended. 

**It is important to note that there is no requirement for these options to be considered or presented consecutively.** 

### Skill-based performance remediation

* [Coaching](/handbook/leadership/underperformance/#coaching)
* [Written Performance Letter](https://about.gitlab.com/handbook/leadership/underperformance/#written-performance-letter)
* [Written Warning](/handbook/leadership/underperformance/#written-warning)
* [Performance Improvement Plan (PIP)](/handbook/leadership/underperformance/#performance-improvement-plan-pip)
* Letting Someone Go

 **Important**: [As noted in the PIP section below](/handbook/leadership/underperformance/#not-the-first-step-to-letting-someone-go), we only implement PIPs if we believe team members have the ability to be successful on the PIP. For team members who truly lack the **skill** for the role even after a period of coaching and support, we would not implement a PIP.


### Will-based performance remediation

* [Coaching](/handbook/leadership/underperformance/#coaching)
* [Verbal Warning](/handbook/leadership/underperformance/#verbal-warning)
* [Written Warning](/handbook/leadership/underperformance/#written-warning)
* Letting Someone Go
 
## Coaching

[Coaching](/handbook/leadership/coaching/) is the preferred option to deal with underperformance and is the first step in addressing performance issues.

Managers are expected to address performance concerns (skill-based or will-based) in a timely manner.  Managers should address concerns verbally during one-on-one meetings or in impromptu private coaching sessions with their team member. These conversations must be documented by the manager and shared with the team member so that everyone has a record of the discussion and is in alignment on where improvements needs to be made and by when. Documentation should be brief (a few key bullet points or a paragraph), and should be sent via email to the team member within 24 hours of the verbal discussion.  See example coaching email below:

Dear(Team member name)

Thank you for meeting with me today to discuss (insert topics  of concerns or issues discussed and any agreed upon actions). 

Please know that I am fully committed to working with you in addressing these issues.  If there is anything I can do to assist you in meeting these expectations, please do not hesitate to let me know. My goal in bringing this matter to your attention is partner together in improving these areas to be successful at GitLab.  

Thanks,
(Manager name)

Underperformance feedback should be the first item on your [1-1 agenda](/handbook/leadership/1-1/suggested-agenda-format). If it is helpful, managers can use the [Managing Underperformance Meeting Plan Tempate](https://docs.google.com/document/d/1dNFrGWS9NNUNrIo8ts9RwXObVB9nTgHaXr-_y2A0ipU/edit#) to facilitate the discussion with clear actionable steps on how to talk through feedback. 

Helping GitLab team members understand clearly how their performance is below the standard expected quickly is very important to foster immediate improvement and continued success. It is also important to clarify when feedback given can provide helpful coaching vs. when to address a skill/will based performance issue. It is not always clear how serious the feedback being provided is and setting the context can be critical.

If there are extenuating circumstances, some leeway may be granted depending on the situation. This is an area where a People Business Partner or the Team Member Relations Specialist can provide a sounding board or a needed perspective. 

When underperformance is detected, managers compensate by checking the team member's work more frequently. The company results should not be affected more than through under-hiring where the position was vacant and others would step in to compensate.

## Verbal Warning

A Verbal Warning is used to bring attention to new or ongoing deficiencies in conduct and/or will-based performance issues. The intent is to define the seriousness of the situation to encourage immediate corrective action.  Not all team members will receive a verbal warning; whether (and/or when) a team member receives a verbal warning may depend on local regulations. If you have any questions about whether you should complete a verbal warning, please reach out to the team member relations group..

The template for a Verbal Warning is found in the confidential PBP resources folder.

Verbal warnings should be communicated in 1:1 meetings, and subsequently confirmed by email with the manager emailing the team member with a recap of the discussion. The manager will then save the documentation to the team member's BambooHR profile, in the Action Notices folder. The verbal warning will remain in the BHR file for the requisite time, as required by applicable local law.

## Written Performance Letter

A Written Performance Letter is used to bring attention to performance issues or ongoing deficiencies previously addressed by your manager.  Team members should already be aware of the performance issues due to ongoing coaching in your 1x1's with your manager and the follow up coaching emails.  The Written Performance Letter will identify development areas/gaps, outline what success looks like and specific action/deliverables and due dates.  The Written Performance Letter is not a formal Performance Improvement Plan (PIP) it is a documented coaching mechanism to help provide the team member with clear guidance to immediately improve performance.  

The team member relations specialist will send the Written Performance Letter to the team member and manager to review and sign via Hellosign. Once signed the team member relations specialist will save the documentation to the team member's BambooHR profile, in the Action Notices folder. There may be times when a team member will not sign a written performance letter, that will be noted on the document and stored in BambooHR. The written performance letter will remain in the BHR file for the requisite time, as required by applicable local law.

The template for a Written Performance Letter is found in the confidential PBP resources folder.

## Written Warning

A Written Warning is used to bring attention to new or ongoing deficiencies in conduct and/or will-based performance. The intent is to define the seriousness of the situation to encourage immediate corrective action.  Not all team members will receive a written warning; whether (and/or when) a team member receives a written warning may depend on local regulations. Managers will work with the Team Member Relations Specialist to determine whether a written warning will be delivered to the team member. 

The manager and team member relsations specialist will inform via a zoom meeting that the team member will be recieving a written warning.  The team member relation specialist will then send the Written Warning Letter via hellosign to the team member to review and sign.  Once signed the team member relations specialist will save the documentation to the team member's BambooHR profile, in the Action Notices folder. There may be times when a team member will not sign a written warning, that will be noted on the document and stored in  BambooHR. The written warning will remain in the BHR file for the requisite time, as required by applicable local law.
 
## Performance Improvement Plan (PIP)

#### Not The First Step To Letting Someone Go

Many companies use a PIP for most involuntary offboarding, as documented support for releasing a team member.
At GitLab we think that giving someone a plan while you intend to let them go is a bad experience for the team member, their manager, and the rest of the team.

A PIP at GitLab is not used to "check the box;" a PIP is a genuine last chance to resolve underperformance.
You should only offer a PIP if you are confident that the team member can successfully complete it.  The team member should also be committed to successfully completing the PIP and maintaining the level of performance arrived at through the PIP.  A PIP will not be successful unless both the team member and the manager believes the team member can succeed.

For team members who truly lack the **skill** for the role or for those team members who lack the **will** to be successful a PIP should not be offered.  PIPs as mentioned above are for team members who with additional guidance, coaching, resources or tools have the ability to improve and sustain long term performance.  

#### SMART Goals
As part of the PIP the manager will work with the team member to define SMART goals. SMART goals allow both the manager and team member to define requirements, track progress, and improve communication of expectations for success during the PIP period.

**SMART** is an acronym that can be used in creating the PIP requirements. Clear and reachable goals should meet the following criteria:

* **Specific:**  Specifically define what you expect the team member to do/deliver.  Avoid generalities and use action verbs as much as possible.
* **Measurable:** You should be able to measure whether the team member is meeting the goals, or not.  Identify how you will measure success - usually stated in terms of quantity, quality, timeliness or cost (e.g. increase by 25%).
* **Achievable:** Make sure that accomplishing the goal is within the team member's realm of authority and capabilities.  While considering whether a goal is actionable/achievable, you also need to consider the team member's total set of goals,.  While each individual goal may be achievable, overall, you may be assigning the team member more goals, than they can reasonably be expected to successfully complete.
* **Realistic:** Can the team member realistically achieve the objectives with the resources available?  Ensure the goal is practical, results-oriented and within the team member's realm of authority and capabilities.  Also, Relevant:  Where appropriate, link the goal to a higher-level departmental or organizational goal, and ensure that the team member understands how their goal and actions contributes to the attainment of the higher level goal.  This give the team member a context for their work.
* **Time-bound:** When does the objective need to be completed?  Specify when the goal needs to be completed (e.g. by the end of the quarter, or every month).

**Sample SMART Goals:**

Bad SMART Goal: "Improve overall qualified sales lead".

Good SMART Goal: "In May, June and July, Jane Doe must have an increase of 20% in overall qualified sales leads entered into Salesforce.com"

Bad SMART Goal: "Increase Fix defects"

Good SMART Goal: "Fix at least 8 defects. Must be fixed with code changes, not closing as "Won't Fix, "Not Reproducible".  All defects must be dev completed/merged by end of business Monday, Jan. 1st, 2018".

#### CREDIT Values Applied To The PIP

Our values should be top of mind in administering a PIP. 

* **Collaboration**: The PIP is an opportunity for manager and the team member to work together towards a desired outcome. Pay attention to the timeline and collaborate on next steps after each milestone is achieved.
* **Results**: The goal of the PIP is to see results, that could be an improvement in performance and/or a changed behavior. Did productivity increase? Are assigned projects being completed on time? Is there an increase in the Sales pipeline? Those results must also be attainable and measurable. 
* **Efficiency**: The manager provides the support needed for the team member to achieve the required results but the ownership and accountability lies with the team member to drive their own performance. The PIP ensures that the team members will correct specific issues identified in a timely manner, under a managed process, showing specific results. Timely manner is dependent on the timeline agreed to by both the Manager and the team member. Typically, a PIP could last 2-4 weeks depending on the role and circumstances.
* **Diversity**: As part of our efforts to foster an environment where everyone can thrive, the PIP should be viewed as a valuable tool designed to address individual behavior. Since each team member is unique, Managers should be mindful that they continue to acknowledge and adjust for diversity of thoughts, differences in communication style and learning preference while following the PIP.
* **Iteration**: Change is expected during the PIP. So small and consistent changes, or iterations, that move the team member toward the final required change should be expected. Positive, effective changes that continue after the PIP should also be encouraged.  Improvement must be maintained.
* **Transparency**: Have regular and honest communication during the PIP. As a manager, set expectations around communication upfront. As a team member, share challenges as they come up. If things are not going well and either party wishes to end the PIP early, be open about that and discuss a mutual separation and any related severance.

#### Experience

It is important to remember that the root cause of issues can be a variety of things, PIPs are not intended to be a negative document. They are an opportunity for the manager and the team member to work together to get the team member back on track. We have an example of this to share here, it is anonymized in line with keeping job feedback private as per the [General Guidelines](/handbook/general-guidelines);

**GitLab team member:**

"Although nobody wants to be put on a PIP, for me it ended up ultimately being a positive experience.  My initial introduction to the plan was a shock and a serious blow to my self confidence, but the process was presented in a fair and open way with clearly defined steps and goals,.  The document presented an attitude of wanting to help me improve and thrive, not a pretext to send me out the door.  This helped me shape my attitude going through the process.  As it turns out I had several blind spots in my communication and time management skills that needed to be remedied, and over the course of the PIP with weekly updates with my manager and some personal efforts in activity logging I was able to improve in both of these areas".

**Manager:**

"For me as a manager, I want to be honest and open with people. I never feel good about telling people they are not meeting the standard. At the same time I really want people to improve. With the PIP we were able to clearly talk about the work that needed to be done to, make them improve and get them where we needed them to be. In this case, the underperformance was not a lack of skills. We merely needed to redirect their focus".

#### PIP Process

The intention of a PIP is to support the team member in any way required to make their time at GitLab a positive experience but also to make clear that immediate and sustained improvement
is required. The Society for Human Resources Management (SHRM) has a [helpful guide](https://www.shrm.org/templatestools/howtoguides/pages/performanceimprovementplan.aspx) to review when
you this step is needed to push past the current performance issues.

A performance improvement plan includes the following:

   * Evaluation of current work
   * Clear direction including metrics and concrete goals, to improve (e.g. finish X before Y)
   * Resources/coaching necessary to achieve goals,

1. Here is a [basic PIP template](https://docs.google.com/document/d/1BMufROj-Oalz5DIXWSjzTLCnvADEUZi7osJuhaFYvHw/edit?usp=sharing) which will provide a good start to creating the document. For an alternative format, you can use this [alternative template](https://docs.google.com/document/d/1c1LGzd83nvXU-JcknuXO72lQjHROPykWVglfPZpZ6kY/edit) Whichever template you choose, it should be customized to fit the particular situation. 
    - Once the [PIP is documented](https://docs.google.com/document/d/1BMufROj-Oalz5DIXWSjzTLCnvADEUZi7osJuhaFYvHw/edit?usp=sharing) schedule a call with the team member outside of the regular 1-1
    - Present the PIP to them on the call and go through it with them. Read it verbatim and don't deviate from it or think of other things to add during the call
    - Re-iterate that you want them to improve and you're there to support them. You will be checking their progress each week (or day if really necessary)
    - Ask them if they have any questions
    - Inform the Team Member Relations Specialist (TMR) that the PIP conversation has taken place and the document have been sent to the team member.
    - Once the PIP has been presented to the team member the TMR will upload in Hellosign and send for the team members signature.  Signing the document does not mean a team member agrees with the PIP but acknowledges that the PIP conversation between the manager and team member has occurred. 
    - Once the PIP is signed and returned, the team member relations specialist will track the start and end date of the PIP in the team member relations tracking tool. 
1. Typically, a team member gets time (2-4 weeks depending on the role and circumstances) to demonstrate improvement and meet specific goals, outlined in the PIP. By design, a PIP is expected to support a successful and sustained improvement in performance.
    - If sufficient improvement is not made, the PIP may end early and we move to next steps in termination (steps may vary by country). 
    - If sufficient improvement is not made but progress is headed in the right direction, a PIP may be extended at the discretion of the manager. 
1. If a team member showed great improvement during the PIP period and has met all expectations as set out in the PIP at the end of the review period, the PIP will close successfully. After the evaluation the TMR will stage the letter of completion in Hellosign for the manager to sign. A template of this letter can be found in the 'PBP Resources' folder. The team member will receive a copy of the signed letter of completion. 
    - If a team member shows performance that falls to an unsatisfactory level during a six month period following the end of your PIP, appropriate action may be taken, up to and including possible offboarding. 
1. If the team member has not shown sufficient improvement and has not met all expectations as set out in the PIP document, the team member is separated or their contract is cancelled. It is not necessary to create a second PIP for the same performance issues within a reasonable period of time and after informing the team member that the unacceptable performance has resurfaced in writing (an email is fine). 
1. To begin the offboarding process, the manager align with the Team Member Relations Specialist and forward a recommendation for offboarding to their Executive team member and People Business Partner including the history of the PIP and the recurring performance issues. If a team member does need to be let go, work with Team Member Relations Specialist to follow the process for [involuntary offboarding](/handbook/people-group/offboarding/#involuntary-offboarding).


***The PIP process is between a manager and their direct report. Information shared in a PIP should be kept confidential by both participants. If underperformance becomes a reason for offboarding, the individual should not be taken by surprise, but others at the company might be.*** 

#### PIP Compensation Impact

It is important to note that when a PIP is active, the team member is not eligible for compensation increases from the [annual compensation review](/handbook/total-rewards/compensation/compensation-review-cycle/) or the [annual refresh grant program](/handbook/stock-options/#refresh-grants).

### Internal Applications and Transfers during underperformance remediation

When in the process of underperformance remediation, such as when a written warning, PEP or PIP is active, you are generally not eligible for transfer to other roles within GitLab. If the underperformance is exclusively a result of a skills mismatch (not of lack of motivation, commitment, delivery etc. or misalignment to our values) other roles within GitLab may be considered, however, a determination regarding your eligibility is at the discretion of your manager and People Business Partner. 

When the underperformance is resolved and you want to transfer to another role, please review [the Internal Application Process](https://about.gitlab.com/handbook/people-group/promotions-transfers/#for-internal-applicants---different-job-family) for transfer. Please note that an internal reference will be conducted before the transfer is final, with this the documented underperformance may be discussed.  

### Talent Assessment and managing underperformance

Managers need to be aware and take into consideration both skill-based and will-based performance issues when they are particpating in the [Talent Assessment Performance/Potential Matrix](https://about.gitlab.com/handbook/people-group/talent-assessment/) process as listed in the handbook.  It is critical that a manager take in these performance issues when evaluating a team member.  If a team member is technically performing in their role but there are will-based performance issues that is impacting their ability or the teams ability to succeed these need to be taken into consideration.  

### Letting Someone Go

This should be discussed with a Team Member Relations Specialist before any action is taken in order to ensure it is done in compliance with local laws and regulation. As soon as you know you'll have to let someone go, do it immediately. The team member is entitled to know where they stand. Delaying it for days or weeks causes problems with confidentiality (finding out that they will be let go), causation (attributing it to another reason), and escalation (the working relationship is probably going downhill).

### Team member leave during an investigation

There may be times when a team member is placed on paid or unpaid leave during an investigation.  This action is taken to protect the team member and GitLab during the investigative process.  During this time, all access to GitLab's data and systems will be suspended.  The purpose of suspending accounts is to minimize risk to GitLab team members, systems, and reputation.  

The investigation is considered high priority to minimize the amount of time the team member is on leave.  Our goal is to complete the investigation in no more than 5 business days. The team member must remain available during this time for further questions or other matters related to the investigation. The leave is meant to enable GitLab to conduct a thorough and speedy investigation and does not in itself carry any implication of prejudgement, nor does it constitute any form of disciplinary action. The leave will only remain in place as long as necessary and can be lifted at any time.  

Please refer to the [Offboarding](https://about.gitlab.com/handbook/people-group/offboarding/#sts=Involuntary%20Offboarding) of the handbook for the process to initiate a leave.

### Underperformance in Senior Leadership

Regardless of level in the organization, the expectation is that all team members are provided with both coaching and feedback prior to a decision to exit the team member. Feedback is critical to development at GitLab at all levels and should be done in real time as much as possible.

However, for VP and higher roles we are unlikely to offer a formal performance remediation plan or place a VP on a formal PIP. If after documented coaching and feedback sessions the performance has not improved GitLab would move to a separation with the team member.

We do this because the impact of their continued underperformance is greater on the rest of the organization.


