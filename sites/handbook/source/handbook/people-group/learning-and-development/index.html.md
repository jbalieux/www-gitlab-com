---
layout: handbook-page-toc
title: Learning & Development
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Introduction

Welcome to the Learning & Development (L&D) page at GitLab! L&D is an essential part of any organization's growth, success, and overall business strategy. We want to support the growth of our GitLab team-members' competencies, skills, and knowledge by providing them with the tools they need and the opportunities to progress their own personal and professional development.  

## How to Communicate with Us

Slack: `#learninganddevelopment`

Email Us: `learning@gitlab.com`

[Request support/Collaborate on new learning content](/handbook/people-group/learning-and-development/work-with-us)


## Mission
Our mission is to provide resources to enable team members to succeed in their current roles and develop new skills to further their professional and personal development. L&D provides adaptive and blended growth and development opportunities, including skills building, career development, and technical training that aligns with GitLab's strategic priorities. L&D's mission is to cultivate a culture of curiosity and continuous learning. 

### GitLab Learning Strategy & Roadmap

<figure class="video_container">
<iframe src="https://docs.google.com/presentation/d/e/2PACX-1vRBdQFDSp8MBSkekUzM-DItdPxr-ETEjrCq85fhET_cC-6nAJGfwuFE-aK3jwSLJylg6yX8N1THaHsk/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
</figure>

**Learn more about our [FY22 Roadmap and curriculum](https://docs.google.com/spreadsheets/d/1DuJ_gWqh0oWyMkMPHKlFs-7eBX98g7oD3QUQfPPzHtQ/edit#gid=1557718375)**.

### Handbook First Training Content

All material in the handbook is considered training. The Learning & Development team pulls content from the handbook to build [handbook first learning content](/handbook/people-group/learning-and-development/interactive-learning). One of L&D's primary responsibilities is to ensure that content lives in the appropriate section in the handbook. In the below video, Sid, explains how the content of courses is not separated from the handbook to the L&D team. 

<figure class="video_container">
  <iframe width="560" height="315" src="https://www.youtube.com/embed/G57Wtt7M_no" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>

### GitLab Learning & Development Principles

1. **Meaningful and relevant content.** We deliver learning solutions that drive team member's development and growth to enable professional goals.

1. **Values aligned.** Our learning solutions reinforce GitLab’s [values](/handbook/values/), and foster continuous learning and curiosity.

1. **Diverse approaches to learning.** We apply a blended learning model for learning solutions that adapt to various learning needs.

1. **Community.** We make our L&D offerings available to the public, aligned to our mission that everyone can contribute.

### Learning and Development Responsibilities

* Set learning strategy to develop and attract GitLab talent through a blend of immersive learning experiences
* Identifies and develops strategic relationships across the organization to motivate and develop team members
* Demonstrate thought leadership and subject matter expertise in learning while applying adult learning theories
* Design learning solutions and experiences in support of organizational values and culture, leadership principles, people manager core capabilities, career development, and more
* Deliver and develop training content to meet our strategic goals
* Perform learning needs analysis with leadership and e-group to understand and execute on learning and development opportunities

### L&D Organization

We are a small team, but we've got a big role to play at GitLab! 

* [Learning and Development Manager](/job-families/people-ops/learning-development/#learning--development-manager): [Josh Zimmerman](/company/team/#Josh_Zimmerman)
* [Learning and Development Generalist](/job-families/people-ops/learning-development-specialist/#learning--development-generalist): [Jacie Bandur](/company/team/#jbandur)
* [Learning and Development Associate](/job-families/people-ops/learning-development-specialist/#learning--development-associate): [Samantha Lee](/company/team/#slee24)
* [Instructional Designer](/job-families/people-ops/instructional-designer/): Role to be filled
* [Learning System Administrator](/job-families/people-ops/learning-system-administrator/): Role to be filled

## FY22 Learning and Development Calendar 

You can view the Learning & Development calendar, as well as other people group team calendars on the [People Group Page](https://about.gitlab.com/handbook/people-group/#team-calendars). 

## Learning Initiatives 

The L&D team hosts and organizes [learning initiatives](/handbook/people-group/learning-and-development/learning-initiatives/) to engage the GitLab team in opportunities to learn. The team is consistently iterating on and adding to these resources!

## Learning Opportunities 

### Certifications

We provide our team members with certifications to demonstrate their knowledge of specific topics. We have [outlined](/handbook/people-group/learning-and-development/certifications/) our current certifications as well as planned and upcoming certifications for the year.

### Growth & Development

Team members can take advantage of up to $10,000 USD per calendar year for [Growth & Development opportunities](/handbook/total-rewards/benefits/general-and-entity-benefits/growth-and-development/). This amount can be used for professional development, certifications, workshops, coaching, and much more!

### Career Development

Everyone's career development is different, but we have [outlined](/handbook/people-group/learning-and-development/career-development/) what it can look like at GitLab. Career development also includes our [GitLab coaching framework](/handbook/leadership/coaching/) to support managers in holding coaching discussions with their team. 

### Language Courses

Please review language course opportunities covered by the [GitLab Growth and Development Policy](/handbook/total-rewards/benefits/general-and-entity-benefits/#growth-and-development-benefit) for more information.

There is also a way to practice foreign languages and ask for advice in several Slack channels, each dedicated to a specific language. You can find all these channels by searching for channels starting with #lang. If you're missing a channel for your target language, feel free to create one and mention it in #whats-happening-at-gitlab so that fellow GitLab team-members can join too!

### Manager Enablement Programs

GitLab has a growing [resource](/handbook/people-group/learning-and-development/manager-development/) to enable all team members transitioning to a manager role. It contains a link to a checklist, readings, and a form to help learning and development customize your development as a manager.

The [Manager Challenge](/handbook/people-group/learning-and-development/manager-challenge/) is one of the first steps to building out a comprehensive manager enablement curriculum for People Leaders. We use daily challenges and the GitLab tool to facilitate async learning in a remote environment. We encourage new and tenured managers to sign up for an upcoming program to develop their ability to lead remote teams while learning from others in a collaborative setting. 

### Self-Paced Learning 

Take advantage of [LinkedIn Learning](/handbook/people-group/learning-and-development/linkedin-learning/), an online educational platform that team members have access too to help develop skills through expert-led course videos. 

### [GitLab Learn](https://gitlab.edcast.com/): EdCast Learning Experience Platform (LXP) 

We are currently in the process of implementing GitLab Learn, our [EdCast Learning Experience Platform (LXP)](/handbook/people-group/learning-and-development/gitlab-learn/). **The LXP launched internally for GitLab team members on 2021-01-11** This system is more than just a Learning Management System, it is a platform to help deliver engaging learning tailored to you and your role. It can direct to handbook pages, make recommendations for learning based on previous consumption, and serve interactive learning content. 

We are also in the process of launching LinkedIn Learning internally for all team members. Stay tuned for more details and the content will be supplemental to GitLab customized content and apply a [Handbook-first approach to interactive learning](/handbook/people-group/learning-and-development/interactive-learning/)

### Mentoring at GitLab

[Mentor relationships](/handbook/people-group/learning-and-development/mentor/) are an opportunity for individuals to learn from someone's personal experience, background, and perpsective. 

### Compliance Courses 

GitLab has several compliance courses and resources. Explore the [Compliance Courses](/handbook/people-group/learning-and-development/compliance-courses) page to learn more.

## New Learning Content at GitLab

We are always working to create more learning content for our team members. If you have a learning request that you would like the Learning & Development team to develop in partnership with your team, please fill out a `learning-and-development-request` issue template in our [issue tracker](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues). Our team will review and set the priority for your request based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

If you have developed the learning content and would like the Learning & Development team to review, fill out a `learning-and-development-review` issue template in our [issue tracker](https://gitlab.com/gitlab-com/people-group/learning-and-development/-/issues). 

Our team will review and set the priority for your content request or review based on the scale and impact across the organization. Learning items that will be applied and used across the company will take priority. 

### How the L&D team prioritizes requests: 
*  Evaluate the strategic impact of the learning session
*  Determine the level of work associated with the learning requirement
*  Assess the impacted audience groups of the session 
*  Identify measures of success 
*  Assess dates of delivery with course schedule and forecast future date

To request support from the L&D, please see the process for [how to work with us](/handbook/people-group/learning-and-development/work-with-us)

### L&D team sprints

The L&D team uses GitLab issue boards to track priorities, organize collaboration, and set due dates based on three-week sprints.

* The L&D team uses [this sprint issue board](hhttps://gitlab.com/groups/gitlab-com/people-group/learning-development/-/boards/1958538) to track priority issues each quarter.
* The `open` list is a queue for issues that need to be addressed, but have not yet been assigned to a sprint, backlog, or priority.
* When a new issue is opened, it should be moved to either the `ld-backlog` or `ld-fyxx-q1-priority` list. This will determine if the issue is a priority for the current quarter, or a backlog issue to be addresses as time allows.
* The team maintains three milestones, one for each upcoming sprint. The sprints are organized with a milestone using the naming mechanism `L&D Sprint # (Date Range)`
* Issues can be moved from the `ld-backlog` or `ld-fyxx-q1-priority` list to the correct sprint when they are ready to be assigned/addressed.
* When an issue is closed, it should remain in the given milestone.
* At the end of the sprint, the milestone can be removed from the board, or the milestone list can be hidden on the issue board.
* The `L&D Requests` list should be used to organize requests for L&D support or courses coming from other teams.
* The `manager-challenge` list holds issues related to each Manager Challenge and can be hidden on the sprint board.
* The `advanced-software-engineering-course` list houses issues related to external software engineer courses can can be hidden on the sprint board.

Additional planning and notes around the sprint board can be found in [this issue.](https://gitlab.com/gitlab-com/people-group/learning-development/general/-/issues/111)

#### Best practices for using sprint boards and issues:

- Apply a burn-down chart with milestones to track time spent
- Create the issue in the sprint where the work starts
- When creating L&D content, apply one issue to one course when developing content. Maintain all course development material in the issue for organization. 
- Epics do not show up in the boards
- Apply a Storyboard template for course development
- Apply labels to manage different work and priorities (leadership requests, prioritized, p1-p3, triage, WIP, Backlog)
- Consider having GitLab team members vote on priority issues to determine need and interest in learning & development content
- Stack rank issues in the board based on priority if possible
- Use the Time Tracking feature on issues to track time spend. When opening a new issue, use the `/estimate` command to set an estimate for the project to be complete. After each working session, use the`/spend` command to track actual time spent.
- Consider using the `new-initiative` issue template when planning a new learning initiative, engagement program, or program idea
- Consider using the `content-scoping` issue template when proposing a new pathway, creating a new course, or building any new learning experience

 

### Top five training content development principles

If you are developing content to meet your learning needs or partnering with the L&D team, here are five key principles to consider when formulating a learning session: 

1. **Know Your Audience** - Analyze and assess the course audience. Ensure that all audience needs are accounted for at every level in the organization you are delivering the training too. 

2. **Define Learning Objectives** - Highlight the learner outcome. Consider developing two to three broad overall statements of what the audience will achieve. 

3. **Break Down Complex Information** - Consider breaking down complex information into easy to digest visuals or text. Reference the handbook but do not be afraid to create a visual representation or use storytelling for the audience.

4. **Engage the Learner** - Adults learn through practice and involvement. Consider using tools to engage learners in a virtual setting like [Mentimeter](https://www.mentimeter.com/) or [Kahoot](https://kahoot.com/business-u/) to stimulate interactivity. Ask the [L&D team](/handbook/people-group/learning-and-development/) for more insight on learning engagement tools. There is a lot you can leverage! 

5. **Implement Blended Learning Course Content** - Give the audience some pre-course work to read and review before the learning session. Use off-the-shelf resources and ensure the content is applicable to what will be covered in the session. Follow up with the audience following the session to gauge how they've applied what they've learned on the job through surveys and questionnaires. 

### Application of Adult Learning Theory

Adults learn differently in the workplace than in traditional learning environments or how they learned growing up. If you are developing training, consider applying principles related to Adult Learning Theories, those include: 

1. **Transformative learning:** The learning experience should aim to change the individual through transformative learning approaches. Start with learning experiences that appeal to your specific audience, and then move to activities that challenge assumptions and points of view.   

2. **Self-directed learning:** Most of the learning that adults do is outside the context of formal training, so there should be an emphasis on augmenting those informal learning experiences. Infuse applications of pre-reads and post-course follow up. Have the participants bring up examples of self-directed learning that they have taken that is related to the training course. 

3. **Experiential learning:** Adults learn through experiences and by doing. When designing a learning experience, apply activities to stimulate learning by doing through role-playing, simulations, virtual labs, case studies, etc. 

4. **Andragogy:** Recognize that adults learn differently than children. Design learning experiences with the assumption that your participants will come to the table with their own set of life experiences and motivations. Adults tend to direct their own learning, tend to learn better by doing, and will want to apply their learning to concrete situations as soon as possible. 

### Developing Learning Objectives

If you are developing training, add learning objectives to the beginning of the content to state a clear training outcome. A clear learning objective describes what the learner will do upon completion of a learning/training activity. Good learning objectives are what you want team members to learn or achieve. 

**Steps to creating learning objectives:** 
1. Identify the level of knowledge necessary to achieve the aim of the training. Use [Bloom's Taxonomy](https://tips.uark.edu/using-blooms-taxonomy/) to assist with writing practical learning objectives. 
2. Select an [action verb](https://www.bu.edu/cme/forms/RSS_forms/tips_for_writing_objectives.pdf). 
3. Create your very own objective
4. Check your objective. Make sure it includes these four pieces: audience, behavior, condition, and degree of mastery
5. Repeat these steps for each objective

**Sample learning objectives:** 
- By the end of the session, team members will be able to describe the steps taken to address underperformance
- Team members will be able to apply the GROW coaching model framework to coaching sessions with members of their team 
- After learning about the high-performance team-building model, team members will be able to determine the steps needed to reach high performance.

### 70-20-10 Model for Learning and Development

The L&D team uses a formula to describe the optimal sources of learning at GitLab. It shows that team members obtain 70 percent of the knowledge from job-related experiences, 20 percent from interactions with others, and 10 percent from formal learning events. The model is intended to show that hands-on experience (70 percent) can be one of the most beneficial for team members because it enables them to discover and refine their job-related skills. Team members learn from others (20 percent) through a variety of activities we offer every week. Lastly, the formula holds that only 10 percent of professional development comes from traditional live learning and eLearning events. 

### Instructional Design 101

The Learning & Development, [Field Enablement](/handbook/sales/field-operations/field-enablement/), and [Professional Services](/handbook/customer-success/professional-services-engineering/) team hosted an Instructional Design 101 session on 2020-10-28 to better understand concepts of adult learning theory and instructional design. Check out the video to learn how to apply instructional design to learning content. 

<figure class="video_container">
<iframe width="560" height="315" src="https://www.youtube.com/embed/Be8oRHp0E84" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
</figure>
 

## Performance Indicators

- **Engagement Survey Growth and Development Score > X%**

Questions related to growth and development on the semi-annual [Engagement Survey](/handbook/people-group/engagement/) have a favorable score. The exact target is to be determined.

- **Rate of internal job promotions > X%**

Total number of [promotions](/handbook/people-group/promotions-transfers/) in a rolling six month period/total number of employees. The target for this is to be determined.

- **12-month voluntary team member turnover related to growth < X%**

This performance indicator is calculated the same as [12 month voluntary team member turnover KPI](/handbook/people-group/people-group-metrics/#team-member-turnover) but uses the number of team members actively choosing to leave GitLab for growth and development related reasons only. The target is to be determined.

- **Exit Survey Data < 5 % of team members exiting GitLab related to L&D**
- **This is my first commit request**

Questions related to Learning & Development opportunities at GitLab through [Exit Survey Data](https://docs.google.com/document/d/1kPmaQwAsTujbclFSH2J2Le-OAPT1BP5JitFyCqe1BO8/edit#)

