---
layout: handbook-page-toc
title: "Engagement"
description: "GitLab uses engagement surveys to allow team members to provide feedback and leadership to gain insight."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

### Engagement Survey Overview

Team Member Engagement is important to the continued success of GitLab making the Engagement Survey an important element within the annual `People Group` calendar.  Engagement strategies have been shown to reduce attrition; improve productivity and efficiency; further embed the company values and ultimately enhance role satisfaction.  

One of the ways that GitLab tracks team member engagement is through the bi-annual `Team Member Engagement Survey` administered by [CultureAmp](https://www.cultureamp.com/) and facilitated by our team of [People Operations Specialists](https://about.gitlab.com/job-families/people-ops/people-operations/#people-operations-specialist) - this program is comprised of a mid-year pulse survey followed by a more intensive full-spectrum survey rolled out in the last quarter of the GitLab Financial Year.  

Engagement surveys are geared toward giving team members an opportunity to provide feedback in terms of their experience within GitLab touching on pertinent elements such as `Company Culture`, `Teamwork`, `Management` and `Wellbeing` among others.  

Feedback from the survey in turn allows Leadership within GitLab to gain insight into what is most important to team members, in particular what the business is doing well and what may need future iteration with the intention of improving the underlying team member experience and ensuring healthy levels of attrition are maintained.

#### Survey Confidentiality 
In an effort to ensure team member confidentiality is upheld, only managers with >5 respondents will be able to view quantitavive results - this is carried across to demographic filtering in teams with population sizes which exceed the minimum too.

#### Survey Sections and Factors
The GitLab Team Member Engagement Survey is comprised of eleven sections each of which is aligned to a specific area of focus otherwise known as a factor.

The survey factors for FY19 through FY21 were as follows with each section including three to eight questions - the most recent survey was comprised of fifty-four questions in total building out the original forty-six to include insight gathering around COVID-19.  The format of the survey was predominantly Likert or Rating Scale with a handful of free text questions or comments toward the end. The favorable score is the percentage of participants who selected 'agree' and 'strongly agree'.
1. Section 01: GitLab Overall
1. Section 02: Company Confidence
1. Section 03: Our Leaders
1. Section 04: Your Manager
1. Section 05: Teamwork
1. Section 06: Your Role
1. Section 07: The Culture
1. Section 08: Growth and Development
1. Section 09: Wellbeing COVID-19
1. Section 10: Action
1. Section 11: Comments (Free Text)

#### Taking Action
Once the GitLab Team Member Engagement Survey has been closed out, reports will be dissementated to the E-Group and Divisional Heads for review i.e. only those with team populations >5.  These will form the basis of discussion when collaboratively compiling the people strategy for year ahead in conjunction with additional data such as attrition reports etc.

##### Timeline for Action Planning

* Each E-Group member will in collaboration with their respective People Business Partner (PBP) to identify one or two acton items to initiate coming out of the results review - these will be tracked and managed within CultureAmp moving forward using the [Action Framework](https://academy.cultureamp.com/hc/en-us/articles/115005387945-Take-action-with-action-framework).
* After action items have been identified at the E-Group level, all managers who recieved an engagement survey result report (AKA: managers with 5+ direct reports who responded to the survey) will also be asked to create an action plan in Culture Amp.  
* A manager may choose to align their action planning to their leader or division or they may choose to add a specific and separate action for their team. This will likely depend on the size/structure of the organization. 
* The due date to have actions in CultureAmp is **February 26, 2021**.

The People Operations Team who facilitate the GitLab Team Member Engagement Survey have compiled a detailed video around the survey results, how to view divisional results directly within Culture Amp and objectively identifying impactful action items from the feedback received:

<figure class="video_container">
  <iframe src="https://drive.google.com/file/d/1x1DYgoDT-Wdm-jJeexhrOpu7Nrt6vLe-/preview" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

#### Resources for Managers

[Managers Guide to Taking Action](https://academy.cultureamp.com/hc/en-us/articles/207302419-Manager-s-guide-to-taking-action-on-Engagement-survey-results)

[Take Action with Action Framework](https://academy.cultureamp.com/hc/en-us/articles/115005387945-Take-action-with-action-framework)

### 2020 Engagement Survey
The GitLab Team Member Engagement Survey for 2020 (FY21) ran from 2020-11-16 to 2020-12-15 - questions and fine details can be found in the [following planning issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/931).

Bar those who joined on or after 2020-11-01 the survey was extended to all GitLab Team members for completion.  Participation for the FY21 survey closed out at 92% with 1,127 team members having taken part.

#### FY21 Engagement Survey Program Timeline
- 2020-11-16 Launch Date
- 2020-12-16 Close Date
- 2020-12-23 High Level Summary Release (Company-Wide)
- 2021-01-08 E-Group Summary Release
- 2021-01-12 Training Video and Handbook Update
- 2021-01-15 to 2021-01-31 Functional Group Action Planning re Focus Areas
- 2021-02-26 Functional Action Plans added to FY21 Q1 OKRs and CultureAmp Action Planning Module added by E-Group leader and managers


#### Benchmarking and Reporting
The FY21 survey was benchmarked against New Tech 1,000+ 2020 in addition to being reviewed against the preceeding years results.  Preliminary reports were released on 2021-01-08 painting a broad overview afterwhich individual divisional reviews will take place in collaboration with the [People Business Partners (PBPs)](https://about.gitlab.com/job-families/people-ops/people-business-partner/) and respective Group Leaders.

#### Company Results (Overall)

| **Favourable** | **Neutral** | **Unfavourable** |
|---|---|---|
| 81% | 14% | 5% |

The benchmarked favourability score for New Tech 1,000+ 2020 was 73% placing GitLab 8% higher than the norm for this particular bracket.

#### Top Three Scores

| **Factor** | **Favourability Rating** |
| --- | --- | 
| Your Manager | 88% | 
| Wellbeing COVID-19 | 85% | 
| Culture | 84% | 


| **Question** | **Favourability Rating** |
| --- | --- |
| I am proud to work for GitLab | 93% |
| I feel supported when I need to take time off or make use of flexible working arrangements | 93% |
| I know how my work contributes to the goals of GitLab | 93% |

GitLab was rated between 5-13% percent higher than the industry benchmark i.e. New Tech +1,000 2020 across all three questions detailed above.

#### Bottom Three Scores

| **Factor** | **Favourability Rating** |
| --- | --- | 
| Our Leaders | 79% | 
| Growth & Development | 70% | 
| Action | 54% | 


| **Question** | **Favourability Rating** |
| --- | --- |
| I have been provided an opportunity to see and discuss prior engagement survey results  | 60% |
| The Learning & Development programs provided by GitLab help me grow my career | 47% |
| I have seen positive changes since the previous engagement survey | 39% |

GitLab scored 5% below the industry benchmark i.e. New Tech +1,000 2020 for the lowest scoring question surrounding positive changes however there was no comparative for the other two questions.

#### 2021 E-Group Focus Areas

- Action (though action is our lowest score we have seen improvement year over year and will continue to focus on this area)
- Growth & Development
- Compensation
- Resource Allocations

#### Questions and Support
The GitLab Team Member Engagement Survey is managed by the `People Operations Specialists`, if you require support or have any questions around the survey please be sure to reach out via `#peopleops`.

## Previous Results (Reverse Chronological)

### 2019 Engagement Survey
The GitLab Team Member Engagement Survey for 2019 (FY20) ran from 2019-10-14 to 2019-11-04 - questions and fine details can be found in the [following planning issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/503).

Bar those who joined on or after 2019-10-01 the survey was extended to all GitLab Team members for completion.  Participation for the FY20 survey closed out at 87% with 812 team members having taken part.

#### Benchmarking and Reporting
The FY20 survey was benchmarked against New Tech Size >500 2019 in addition to being reviewed against the preceeding years results.  Preliminary reports were released in the week of 2019-11-07 painting a broad overview afterwhich individual divisional reviews will take place in collaboration with the [People Business Partners (PBPs)](https://about.gitlab.com/job-families/people-ops/people-business-partner/) and respective Group Leaders.

#### Company Results (Overall)

| **Favourable** | **Neutral** | **Unfavourable** |
|---|---|---|
| 88% | 9% | 3% |

A benchmarked favourability score for New Tech Size >500 2019 was not available for this particular survey.

#### Top Three Scores

| **Factor** | **Favourability Rating** |
| --- | --- | 
| Culture | 87% | 
| Your Manager | 87% | 
| Teamwork | 86% | 


| **Question** | **Favourability Rating** |
| --- | --- |
| I would recommend GitLab as a great place to work | 95% |
| GitLab is in a position to really succeed over the next three years | 94% |
| I am proud to work for GitLab | 94% |

GitLab was rated between 11-17% higher than the industry benchmark i.e. New Tech Size >500 2019 across all three questions detailed above.

#### Bottom Three Scores

| **Factor** | **Favourability Rating** |
| --- | --- | 
| Your Role | 84% | 
| Growth & Development | 80% | 
| Action | 46% | 


| **Question** | **Favourability Rating** |
| --- | --- |
| I believe action will take place as a result of this survey  | 64% |
| I have been provided an opportunity to see and discuss prior engagement survey results | 46% |
| I have seen positive changes since the previous engagement survey | 29% |

GitLab scored 15% below the industry benchmark i.e. New Tech Size >500 2019 for the lowest scoring question surrounding positive changes however there was no comparative for the other two questions.

#### 2019 E-Group Focus Areas
- Taking action from areas guided by the GitLab Team Member Engagement Survey Results.
- Managerial encouragement of Team Member development.
- Total Compensation.

### 2018 Engagement Survey
The GitLab Team Member Engagement Survey for 2018 (FY19) ran from 2018-10-14 to 2018-11-04 - questions and fine details can be found in the [following planning issue](https://gitlab.com/gitlab-com/people-group/General/-/issues/240).

Bar those who joined on or after 2018-08-17 the survey was extended to all GitLab Team members for completion.  Participation for the FY19 survey closed out at 94% with 308 team members having taken part.

#### Benchmarking and Reporting
The FY19 survey was benchmarked against 2018 New Tech Size - Large Companies (500+).  Preliminary reports were released in the week of 2018-11-06 painting a broad overview afterwhich individual divisional reviews will take place in collaboration with the [People Business Partners (PBPs)](https://about.gitlab.com/job-families/people-ops/people-business-partner/) and respective Group Leaders.

#### Company Results (Overall)

| **Favourable** | **Neutral** | **Unfavourable** |
|---|---|---|
| 83% | 12% | 5% |

The benchmarked favourability score for 2018 New Tech Size - Large Companies (500+) was noted at 70% i.e. 13% lower than that of GitLab overall.

### Top Scoring Factors

| **Factor** | **Favourability Rating** |
| --- | --- | 
| Company Confidence | 84% | 
| Management | 84% |
| Collaboration and Communication | 84% | 
| Work / Life Balance | 84% | 

| **Question** | **Favourability Rating** |
| --- | --- |
| I am proud to work for GitLab | 95% |
| I know how my work contributes to the goals of GitLab | 94% |
| GitLab is really in a position to succeed over the next three years | 93% |

GitLab was rated between 4-14% percent higher than the industry benchmark i.e. 2018 New Tech Size - Large Companies (500+) across all three questions detailed above.

### Bottom Three Scores

| **Factor** | **Favourability Rating** |
| --- | --- | 
| Alignment and Involvement | 74% | 
| Feedback and Recognition | 63% | 
| Action | 41% | 


| **Question** | **Favourability Rating** |
| --- | --- |
| I believe my total compensation (base salary+any bonus+benefits+equity) is fair relative to similar roles at other companies | 43% |
| My manager or someone else has communicated clear actions based on recent team member survey results | 33% |
| I have seen positive changes taking place based on recent employee survey results | 32% |

GitLab scored 12% below the industry benchmark i.e. 2018 New Tech Size - Large Companies (500+) for the lowest scoring question surrounding positive changes however there was no comparative for the other two questions.

#### 2018 E-Group Focus Areas
- Taking action from areas guided by the GitLab Team Member Engagement Survey Results.
- Managerial encouragement of Team Member development.
- Total Compensation.
