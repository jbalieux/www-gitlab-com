---
layout: handbook-page-toc
title: "Celebrations and Significant Life Events"
description: "Review different ways GitLab celebrates its team members."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Celebrations

### GitLab Anniversary
At GitLab we like to acknowledge and celebrate team member milestones and contributions.  This means that those celebrating their first, third, fifth and tenth hire-date anniversary will receive a token of acknowledgement.

Along with this, all celebrants regardless of length of tenure will receive a mention in the **#team-member-updates** channel which allows the greater GitLab community to share in the celebration!

#### Anniversary Gifts
On the last day of the month of celebration, team members can anticipate an email with a unique link which will lead them to the GitLab Anniversary Gift ordering portal where they can make the relevant size and shipping selections (where applicable).

The average shipping time taking into account current restrictions as a result of global events is three weeks.  In an instance where an order has been placed and not received we encourage team members to reach out to the People Experience Team in the **#anniversary-celebrants** channel.

| Year | Gift | 
| --- | --- | 
| **Year 01** | Celebratory Confetti Tanuki Socks (Unique to the Anniversary Program) |
| **Year 03** | GitLab Tanuki Vest |
| **Year 05** | Travel Bag / Backpack |
| **Year 10** | $500 Travel Grant |

### Team Member Birthdays
GitLab encourages team members to take a day of vacation on their special day in alignment with our **[Paid Time Off Policy](https://about.gitlab.com/handbook/paid-time-off/#a-gitlab-team-members-guide-to-time-off)**.  If your celebration happens to fall over a weekend please be sure to take an alternate day such as the Friday prior or the Monday after.

Kindly note that team members are welcome to send cards or tokens of acknowledgement to celebrants in their personal capacity however the People Experience Team is unable to do so on behalf of GitLab; a Team or an Individual Team Member.

## Significant Life Events
### Flowers and Gift Ordering
Managers are able to send Gifts and Flowers on behalf of their team members in acknowledgment of significant life events such as the birth of a little one; well wishes ahead of surgery, or the loss of a loved one. Generally we wouldn't send over gifts or flowers for (public) holidays that would not relate to a significant life event. If ever in doubt reach out to your manager to check before purchasing any gifts or flowers. 

The event in question must pertain to a GitLab Team Member or the immediate family of a GitLab Team Member and will be allocated to the respective team members departmental budget - the spend range for significant life events is **$75 to $125**. 

Managers can facilitate the ordering of Flowers or Gifts for delivery, but please note that you are unable to send restaurant gift cards at this time.  In an instance where you would like to extend the offer of a meal or food delivery service, this will need to be expensed by the recipient for reimbursement.

Recommended sites:
- [1-800 Flowers](https://www.1800flowers.com/) 
- [Flora Queen](https://www.floraqueen.com/) 

The budget for sending these gifts will come from the department budget of the gift-receiving team member. 

To make sure the reports in Expensify are routed correctly, managers will need to be sure to fill in the correct division and department. Managers will also need to be sure to use `FY21EmployeeGiftsFlowers` in the `classification` section of the expense.  

For any further questions regarding expensing of flowers or gifts, please reach out to the AP team at `ap@gitlab.com`

** Please note: Only managers will be able to view and access their direct reports addresses via BambooHR. This information should never be shared with anyone without prior permission of the team member. 




