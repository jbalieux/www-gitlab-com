---
layout: handbook-page-toc
title: "Engineering Productivity team"
description: "The Engineering Productivity team increases productivity of GitLab team members and contributors by shortening feedback loops and improving workflow efficiency for GitLab projects."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## Common Links

| **GitLab Team Handle** | [`@gl-quality/eng-prod`](https://gitlab.com/gl-quality/eng-prod) |
| **Slack Channel** | [#g_qe_engineering-productivity](https://gitlab.slack.com/archives/CMA7DQJRX) |
| **Team Boards** | [Team Board](https://gitlab.com/groups/gitlab-org/-/boards/978615) & [Priority Board](https://gitlab.com/groups/gitlab-org/-/boards/1333450) |
| **Issue Tracker** | [quality/team-tasks](https://gitlab.com/gitlab-org/quality/team-tasks/issues/) |

## Vision

The Engineering Productivity team increases productivity of GitLab team members and contributors by shortening feedback loops and improving workflow efficiency for GitLab projects. The team uses a quantified approach to identify improvements and measure results of changes.

### Mission

[Reduce pipeline cost](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#average-cost-per-merge-request-pipeline-for-gitlab), [reduce pipeline duration](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#average-merge-request-pipeline-duration-for-gitlab), and [increase pipeline stability](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#gitlab-project-master-pipeline-success-rate) for GitLab projects focusing on projects with the largest reach, leveraging GitLab features where possible.

Improve engineering workflow automation to [decrease mean time to merge](https://about.gitlab.com/handbook/engineering/development/performance-indicators/#mean-time-to-merge-mttm), [decrease ~"bug" open past SLO](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#s1s2-open-bugs-past-target-slo), and [time to triage for new issues](https://about.gitlab.com/handbook/engineering/quality/performance-indicators/#new-issue-first-triage-slo).

Enable frequent and positive experience of Community Contributions from the Wider GitLab Community.

### Workstream objectives

The Engineering Productivity team focuses on the following workstreams and the associated Epics with workstream specific vision and objectives.

| Tracking Label | Epics |
| --- | --- |
| ~"ep::pipeline" | [GitLab Project Pipeline Improvement](https://gitlab.com/groups/gitlab-org/-/epics/1853)<br />[GitLab Project Selective Test Execution](https://gitlab.com/groups/gitlab-org/-/epics/3806) |
| ~"ep::review-apps" | [Improve Review Apps Reliability](https://gitlab.com/groups/gitlab-org/-/epics/605)<br />[Improve Review Apps setup and usefulness](https://gitlab.com/groups/gitlab-org/-/epics/606) |
| ~"ep::triage" | [Quality: Triage](https://gitlab.com/groups/gitlab-org/-/epics/1461) |
| ~"ep::metrics" | [Centralized handbook first metrics dashboard](https://gitlab.com/groups/gitlab-org/-/epics/3580) |
| ~"ep::workflow" | [Reviewer Roulette Improvements](https://gitlab.com/groups/gitlab-org/-/epics/3287)<br /> |


## Areas of Responsibility

* **See it and find it**: Build automated measurements and dashboards to gain insights into the productivity of the Engineering organization to identify opportunities for improvement
  * Implement new measurements to provide visibility into improvement opportunities
  * Collaborate with other Engineering teams to provide visualizations for measurement objectives
  * Improve existing performance indicators
* **Do it for internal team**: Increase contributor and developer productivity by making measurement-driven improvements to the development tools / workflow / processes, then monitor the results, and iterate.
  * Identify and implement quantifiable improvement opportunities with proposals and hypothesis for metric improvements
  * Automated [merge request quality checks](https://docs.gitlab.com/ee/development/dangerbot.html) and [code quality checks](https://docs.gitlab.com/ee/development/contributing/style_guides.html)
  * [GitLab project pipeline](https://docs.gitlab.com/ee/development/pipelines.html) improvements to improve efficiency, quality or duration
* **Dogfood use**: Dogfood GitLab product features to improve developer workflow and provide feedback to product teams
  * Use new features from related product groups (Analytics, Monitor, Testing).
  * Improve usage of [Review Apps] for GitLab development and testing.
* **Engineering support**: Participate in activities related to Engineering throughput and [Quality KPIs](/handbook/engineering/quality/performance-indicators/)
  * [#master-broken](/handbook/engineering/workflow/#broken-master) pipeline monitoring
  * KPI corrective actions such as review app stabilization
  * Quality department pipeline on-call
  * [Merge Request coach](https://about.gitlab.com/job-families/expert/merge-request-coach/) for ~"Community contribution" merge requests
* **Engineering workflow**: Develop automated processes for improving label classification hygiene in support of product and engineering workflows.
  * Automated issues and merge requests triage.
  * Improvements to the labelling classification and automation used to support Engineering measurements.
  * See the [GitLab Triage], [GitLab triage operations], and [GitLab triage serverless] projects for examples.
* **Do it for wider community**: Increase efficiency for wider GitLab Community contributions
* **Dogfood build**: Enhance and add new features to the GitLab product to improve engineer productivity

## Team Members

<%= direct_team(manager_role: 'Backend Engineering Manager, Engineering Productivity') %>

## Work prioritization

The Engineering Productivity team uses [modified prioritization and planning guidelines](prioritization.html) for targeting work within a Milestone.

## Projects

1. [GitLab] CI Pipeline configuration optimization and stability.
1. [GitLab] [Review apps] provisioning.
1. [GitLab Insights] (native) and [Development department metrics](/handbook/engineering/development/performance-indicators/groups) for measurements of Quality and Productivity.
1. [GitLab triage operations] for issues, merge requests, community contributions.
1. [GitLab triage reactive (aka serverless) operations] for issues, and merge requests.
1. [GitLab Triage] engine maintenance.
1. [GitLab Docs] (to be moved to a dedicated team in UX)

## Engineering productivity team metrics

The Engineering Productivity team creates metrics in the following sources to aid in operational reporting.

- [Sisense Quality Engineering KPIs](https://app.periscopedata.com/app/gitlab/516343/Quality-KPIs)
- [Sisense Engineering Productivity Sandbox](https://app.periscopedata.com/app/gitlab/496118/Engineering-Productivity-Sandbox)
- [Sisense Engineering Productivity Pipeline](https://app.periscopedata.com/app/gitlab/564156/Engineering-Productivity---Pipeline)
- [Sisense Engineering Productivity Pipeline Durations](https://app.periscopedata.com/app/gitlab/652085/Engineering-Productivity---Pipeline-Build-Durations)
- [Sisense Engineering Productivity Package And QA Durations](https://app.periscopedata.com/app/gitlab/869271/Engineering-Productivity---Package-And-QA-Durations)
- [Sisense GitLab Issue Triage Dashboard](https://app.periscopedata.com/app/gitlab/621211/WIP:-GitLab-Issue-Triage-Dashboard)
- [GitLab-Org Native Insights](https://gitlab.com/groups/gitlab-org/-/insights)
- [Review Apps monitoring dashboard](https://app.google.stackdriver.com/dashboards/6798952013815386466?project=gitlab-review-apps)
  - [Failed deployment Tiller logs](https://console.cloud.google.com/logs/viewer?authuser=0&interval=P1D&project=gitlab-review-apps&minLogLevel=0&expandAll=false&timestamp=2019-10-11T03:56:06.077000000Z&customFacets=&limitCustomFacetWidth=true&resource=k8s_cluster&advancedFilter=resource.type%3D%22k8s_container%22%0AlogName%3D%22projects%2Fgitlab-review-apps%2Flogs%2Fstderr%22%0Aresource.labels.project_id%3D%22gitlab-review-apps%22%0Aresource.labels.location%3D%22us-central1-b%22%0Aresource.labels.cluster_name%3D%22review-apps-ee%22%0Aresource.labels.namespace_name%3D%22review-apps-ee%22%0Aresource.labels.pod_name:%22tiller-deploy%22%0AtextPayload:(%22%5Btiller%5D%22%20AND%20(%22warning:%20Upgrade%22%20OR%20%22warning:%20Release%22))&scrollTimestamp=2019-10-11T02:10:10.956872965Z&dateRangeStart=2019-10-10T03:58:45.648Z&dateRangeEnd=2019-10-11T03:58:45.648Z)
- [Deprecated Quality Dashboard](https://quality-dashboard.gitlap.com/groups/gitlab-org)

## Communication guidelines

The Engineering Productivity team will make changes which can create notification spikes or new behavior for
GitLab contributors. The team will follow these guidelines in the spirit of [GitLab's Internal Communication Guidelines](/handbook/communication/internal-communications).

### Pipeline changes

#### Critical pipeline changes

Pipeline changes that have the potential to have an impact on the GitLab.com infrastructure should follow the [Change Management](/handbook/engineering/infrastructure/change-management) process.

Pipeline changes that meet the following criteria must follow the [Criticality 3](/handbook/engineering/infrastructure/change-management/#criticality-3) process:

- update of the `CI_PRE_CLONE_SCRIPT` CI variable
- update to the [`cache-repo` job](https://gitlab.com/gitlab-org/gitlab/-/blob/master/.gitlab/ci/cache-repo.gitlab-ci.yml) job

These kind of changes [led to production issues in the past](https://gitlab.com/gitlab-com/gl-infra/production/-/issues/3013).

#### Non-critical pipeline changes

The team will communicate significant pipeline changes to [`#development`](https://gitlab.slack.com/messages/C02PF508L) in Slack and the Engineering Week in Review.

Pipeline changes that meet the following criteria will be communicated:

- addition, removal, renaming, parallelization of jobs
- changes to the conditions to run jobs
- changes to pipeline DAG structure

Other pipeline changes will be communicated based on the team's discretion.

### Automated triage policies

Be sure to give a heads-up to `#development`,`#eng-managers`,`#product`, `#ux` Slack channels
and the Engineering week in review when an automation is expected to triage more
than 50 notifications or change policies that a large stakeholder group use (e.g. team-triage report).

## Experiments

This is a list of Engineering Productivity experiments where we identify an opportunity, form a hypothesis and experiment to test the hypothesis.

| Experiment | Status | Hypothesis | Feedback Issue or Findings |
| --- | --- | --- | --- |
| [Enabling developers to run failed specs locally](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/58569) | In Progress | Enabling developers to run failed specs locally will lead to less pipelines per merge request and improved productivity from being able to fix regressions more quickly | <https://gitlab.com/gitlab-org/gitlab/-/issues/327660> |
| [Use dynamic analysis to streamline test execution](https://gitlab.com/gitlab-org/gitlab/-/issues/222369) | Complete | Dynamic analysis can reduce the amount of specs that are needed for MR pipelines without causing significant disruption to master stability | [Miss rate of 10%](https://gitlab.com/gitlab-org/gitlab/-/issues/222369#note_480768617) would cause a large impact to master stability. Look to leverage dynamic mapping with local developer tooling. Added [documentation](https://docs.gitlab.com/ee/development/pipelines.html#rspec-minimal-job-experiment) from the experiment. |
| [Using timezone for Reviewer Roulette suggestions](https://gitlab.com/gitlab-org/gitlab/-/merge_requests/34862) | Complete - Reverted | Using timezone in Reviewer Roulette suggestions will lead to a reduction in the mean time to merge | Reviewer Burden was inconsistently applied and specific reviewers were getting too many reviews compared to others. More details in the [experiment issue](https://gitlab.com/gitlab-org/quality/team-tasks/-/issues/563#note_397680373) and [feedback issue](https://gitlab.com/gitlab-org/gitlab/-/issues/227123) |

[GitLab]: https://gitlab.com/gitlab-org/gitlab
[GitLab Insights]: https://gitlab.com/gitlab-org/gitlab-insights
[GitLab Triage]: https://gitlab.com/gitlab-org/gitlab-triage
[GitLab triage operations]: https://gitlab.com/gitlab-org/quality/triage-ops
[GitLab triage reactive (aka serverless) operations]: https://gitlab.com/gitlab-org/quality/triage-serverless
[Review apps]: https://docs.gitlab.com/ee/development/testing_guide/review_apps.html
[GitLab triage serverless]: https://gitlab.com/gitlab-org/quality/triage-serverless
[GitLab docs]: https://gitlab.com/gitlab-org/gitlab-docs
