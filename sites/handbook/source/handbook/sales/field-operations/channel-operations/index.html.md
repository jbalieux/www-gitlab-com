---
layout: markdown_page
title: "Channel Operations"
description: "This page serves as the Channel Operations team page and includes standard channel reporting links, and details for managing Channel opportunities"
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
 {:toc .hidden-md .hidden-lg}
 
 
<link rel="stylesheet" type="text/css" href="/stylesheets/biztech.css" />
{::options parse_block_html="true" /}



# Welcome to the Channel Operations page

## _Pinned Announcement_
With the new partner program changes that went into effect on May 3, 2021, the Channel Operations Team created a brief document for internal GitLab Team Members to answer a few basic questions about the system and operational changes to go along with the update. **To view this FAQ, [click here](https://docs.google.com/document/d/1USbuKSzO4Mubd88gYPZI9AUU_zsVJlx3SsfwF-oHX1o/edit?usp=sharing).** For information about the new program details (not operational), [click here](https://about.gitlab.com/handbook/resellers/).

## Meet the Team
### Who We Are
 - Colleen Farris - Director, Channel Operations
 - Emily Murphy  - Manager, Alliance Operations
 - Kim Stagg - Manager, Channel Operations
 - Niles Jamshaid - Manager, Sales Operations
 - Dennis Zeissner - Associate Partner Operations Analyst
 
### How to Contact Us
The #channel-programs-ops Slack channel is monitored by several teams to provide direction and guidance. The entire Channel Operations team can be reached via this channel.

### The Channel Operations Issue Board
The channel operations issue board can be found [here](https://gitlab.com/groups/gitlab-com/-/boards/2552402?&label_name[]=Channel%20Ops). Each column represents a type of request (feature request, alliances, data & reporting, etc.). When you submit a request to the Channel Operations board, the team will assign the issue and add the corresponding tags. 
The Channel Operations Board also uses progress tags on issues to show the status of open issues. Each issue is updated regularly with notes and progress tags and should be checked before reaching out to the team for status updates. 

To open an issue with Channel Operations, [visit this link](https://gitlab.com/gitlab-com/sales-team/field-operations/channel-operations/-/issues) and choose “New Issue.”


## Standard Channel Reporting Links

 
<details>
<summary markdown='span'>
Channel & Alliance SFDC Dashboards
</summary>
- [Global Channel Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYpV): Dashboard that contains standard metrics for measuring overall Channel performance
- [Channel Forecast Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYLJ): Dashboard that contains comparison between `Partner Sourced and SQS = Channel` pipe and bookings
- [Channel Commercial Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYe4): Dashboard that is focused specifically on our Commercial segment
- [Alliances Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYAp): Dashboard that contains standard metrics for measuring Alliance Partner performance
 
</details>
 
<details>
<summary markdown='span'>
Google Sheet Reporting
</summary>
- [Channel & Alliances Weekly Forecast Call Analysis](https://docs.google.com/spreadsheets/d/1t_9VbB_5vjMOAL_Rs7syPCJ4D8Argl6f6Tv39yuNDN0/edit#gid=1119532428): Standard metrics for the weekly Sales Forecast Call, updated weekly
- [GitLab Certification Tracking Dashboard](https://docs.google.com/spreadsheets/d/147DUeV4k2ybqftcnJcBR6SOoZWxGpjoCfNyn1hNVfAg/edit#gid=569795884): Dashboard showing Partner certification data, updated weekly
 
</details>
 
<details>
<summary markdown='span'>
Sisense Reporting
</summary>
- [Channel Analysis Dashboard](https://app.periscopedata.com/app/gitlab/820617/WIP---Channel-Analysis-Dashboard): Overall Global Channel Performance specifically focused on Partner Sourced opportunities
</details>
 
### Reports by Territory
 
<details>
<summary markdown='span'>
AMER
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxE)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxJ)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxO)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxT)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxY)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxd)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqxs)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004aqy2)
</details>
 
<details>
<summary markdown='span'>
APAC
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al8o)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al93)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9I)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9X)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9r)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alAk)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alB4)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBT)
</details>
 
<details>
<summary markdown='span'>
EMEA
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al8t)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al98)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9N)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9c)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9w)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alAp)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alB9)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBY)
</details>
 
<details>
<summary markdown='span'>
PUB-SEC
</summary>
- [Channel Book & Pipe - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al8y)
- [Channel Book & Pipe by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9D)
- [Deal Path Pipe & Book - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9S)
- [Open Partner Sourced Pipeline by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004al9h)
- [Partner Sourced Bookings by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alA1)
- [Partner Sourced Creation by Partner - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alAu)
- [Partner Sourced New Logos - Current Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBJ)
- [Channel Renewals - Current & Next Fiscal Quarter](https://gitlab.my.salesforce.com/00O4M000004alBd)
</details>

For a global view of current and next fiscal quarter channel renewals, [click here](https://gitlab.my.salesforce.com/00O4M000004aWY3).
_All required team reporting is included above. In the event you need a special report, please [open an issue](https://gitlab.com/gitlab-com/sales-team/field-operations/channel-operations/-/issues) on the Channel Operations Board._
_For the reporting training video hosted by the Channel Operations team, as well as quick reference “cheats” to help with your Salesforce reporting, [click here](https://docs.google.com/document/d/1F1O3BUX80SJIqaFD9TF2RuyZ7DXB865AuiwaGctHnGo/edit?usp=sharing)._

## Standard Channel Practices
For detailed information on GitLab’s Channel Partner Program, visit the Channel Partner Handbook [here](https://about.gitlab.com/handbook/resellers/). Partners must be an Authorized GitLab Partner _and have completed one sales certification_ to transact any GitLab products or services. To achieve authorization, partners must have an executed agreement and meet the requirements of the GitLab Partner Program. Only GitLab partners in good standing may sell GitLab products and services unless specifically approved on a case-by-case basis by the GitLab partner program team. Partners can sign up to become authorized [here](https://partners.gitlab.com/).

### Policy and Process
All **Partner Soured** and **Partner Assist** channel opportunities require a Partner to submit a Deal Registration via the Partner Portal in order to receive programmatic discounts. Fulfillment channel opportunities do not hold this requirement. In the event that a Partner does not submit a Deal Registration (excluding: Alliances and GSIs), but it is a Partner Sourced deal, the logic needs to match `Sales Qualified Source = Channel Generated` on the opportunity. For more details on the partner deal registration process and program, click [here](https://about.gitlab.com/handbook/resellers/#the-deal-registration-program-overview).

### Transacting Through Distribution
As of May 3, 2021, all partners in India must transact through our distribution partner, Redington.

### Quoting Requirements for Channel Deals
Any time a deal is being transacted via the Channel, a GitLab quote is **<span style="text-decoration:underline;">required</span>** to be created in SFDC if any pricing is being offered other than MSRP (List Price) or the programmatic fulfillment discount.

At a minimum, a [Draft Proposal](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#how-to-create-a-draft-proposal) needs to be created and provided to the Partner. If a Partner has an approved Deal Registration, then an approved quote needs to be created and provided to that Partner before they place an order.

Discounted quotes that are not in the system and sent to a Partner are not permitted. This includes providing product and pricing quotes details in email. This applies to all GEOs and Segments.

Only GitLab-authorized partners with at least one sales certification are able to receive a discounted quote and transact GitLab products. 

### SFDC Field Definitions
*   _DR - Partner:_ The partner that submitted the Deal Registration.
*   _DR - Partner Deal Type:_
    *   _MSP:_ The partner purchases _and owns_ the license on behalf of the customer. For more information, [click here](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#managed-service-provider-msp-opportunities).
    *   _Resale:_ The partner is actually transacting the deal on their paper.
    *   _Referral:_ The partner is bringing us the lead/opportunity but will either transact direct with GitLab or through another partner.
    *   _Services Attach:_ Partner-delivered services provided to the end user related to their use of the GitLab software.
*   _DR - Partner Engagement:_ How the deal was sourced or the value the partner is bringing.
    *   _Partner Sourced:_ The partner has either found the original opportunity or it is an upsell to a current customer. If the Initial Source = Channel Qualified Lead or `Sales Qualified Source = Channel Generated', then the deal is Partner Sourced.
        *   *In FY21, the Channel Team use "PIO" instead of Partner Sourced. The definition has been updated for FY22.
    *   _Assisted:_ GitLab-sourced opportunity where the partner assists our sales team in closing the deal.
    *   _Fulfillment:_ Partner only processes the order and doesn’t provide additional support to close the deal.
*   _Distributor:_ The GitLab-authorized distributor from which the _DR - Partner_ is buying.
*   _Fulfillment Partner:_ Only applicable if the _DR - Partner Deal Type_ is "Referral" and the deal is being transacted through another partner.
*   _Platform Partner:_ Customer's platform that GitLab is being deployed.
*   _Influence Partner:_ Other partners, generally SI/GSIs or alliance partners, that have helped influence a deal.

_For more details on Partner Engagement definitions go [here](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-engagement-types)._


## Channel Reporting and Tagging
![Reporting_Tagging_Matrix_FY22Q2](/handbook/sales/field-operations/channel-operations/images/Reporting_Tagging_Matrix_FY22Q2.png)

### Definitions
1. _Deal Path_: How the deal is transacted. Values can be Channel, Direct, or Web. Also, includes Referral Ops for Channel.
2. _Deal Reg_: Partner submits a registration for their opportunity via the Partner Portal. For the purposes of this matrix the assumption is the Deal Reg is approved.
3. _Initial Source_: SFDC Lead value that is populated based on lead source. Will default to CQL when a Partner submits a Deal Reg and an Opportunity does not already exist in the system.
4. _SQS_: Who converts/creates the Opportunity in SFDC. **Can only be one value**.
5. _DR - Partner Engagement_: Partner value on the deal via the Partner Program definitions. This is manually selected in most cases.
6. _Order Type_: Customer order designation in SFDC. New First Order or Growth.


### Use Cases

1. **1 and 3**
    *   Channel submits Deal Reg and no Opportunity exists in the system. Therefore the Initial source is CQL, and SQS and DR-Partner Engagement default to Channel and Partner Sourced.
        *   This applies to both New and Growth orders.
2. **2 and 4** 
    *   AE Creates Opportunity prior to Deal Reg being submitted - **CAM to escalate for exception**.
    *   Opportunity stalled and Channel helps to drive to closure - **If channel is simply unsticking an open opp then this is technically Assist. Exceptions can be reviewed**.
    *   Aged opportunities that are closed and revived due to Channel - **Automated clean up with Sales Ops stale Opp policy** **- Exception:** In the event an exception is needed per the scenarios below and exception can be submitted for review and have the ability “override” and restate these are Channel SQS.

### Default Logic
1. `Deal Reg = True` and no Opp Exists then `Initial Source = CQL` > `SQS = Channel`, defaults to Partner Sourced.
2. Alliances: Does not have same logic and currently need to be reported separately.


## Deal Registration

### Rules of Engagement for Deal Registration

*   In order to transact with GitLab, a partner must both be authorized by GitLab, and have completed at least one sales training. To receive a quote with a Partner Sourced or Partner Assist discount, they must complete a deal registration for the opportunity.
*   Only one partner can earn a deal registration discount per opportunity. Partners, other than the partner granted the deal registration discount that requests a quote, will receive the fulfillment discount rate. Deal registration approval is based upon order of receipt of the registration, qualification of the opportunity, partner ability to deliver in-country/region support, and partner relationship with customer. Final deal registration approval decision will be made by GitLab Channel after discussion with GitLab Sales.
*   Any partner opportunity can be a registered deal. These opportunities can be either Partner Sourced, Partner Assist, Partner Fulfilled or Partner Services Attach. Visit [Program and Incentive Definitions](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#program-and-incentive-definitions) to learn what deals qualify for each category.
*   Approved deal registrations have standard 90-day expiration from the date of original approval (Deal Registration extensions beyond the initial 90 days approval are at the sole discretion of GitLab).
*   GitLab collaborates with partners holding the approved deal registration and is available to support partners throughout the entire sales process.
*   In the event the engagement is dissolved, the GitLab Sales Rep will generally notify the partner by phone or email. GitLab will reconsider other qualified deal registrations submitted for this deal, in chronological order of submission. If there are no other registration requests submitted, the GitLab sales rep will typically initiate engagement with a reseller of the GitLab sales rep’s choosing.

### Guidance for Deal Registration Processing
_Why is this important?_



*   Consistent and predictable partner experience
*   Efficient, simple, and objective approval process
*   Uphold integrity of deal reg reporting integrity
*   Mitigates liability
*   Reinforces our Transparency value

### Deal Registration Process
_Note: The Partner Portal is Impartner and  has SSO enabled with Vartopia, the partner-facing Deal Registration system. GitLab uses a third-party managed services team to help manage administrative processes for Deal Registration._

When a partner submits a Deal Registration, the Managed Services team will handle the initial submission. Upon submission, the following things happen:



1. An email is sent to the partner acknowledging that the deal registration has been successfully created.
2. The system notifies the Managed Services team to review the registration.
3. The system creates a SFDC record on the registration object that includes all of the details of the registration.

The Managed Services team will evaluate the Registration, link it to the appropriate Customer account and contact in SFDC, and then send an email to the applicable Channel Manager to approve/deny the registration.

### Deal Registration Instructions/How to Manage a Deal Registration
The Channel Managers need to review the deal registration and either approve or deny. It is highly recommended to work with the applicable GitLab Sales Rep prior to taking action.

While multiple partners can submit a deal registration for an opportunity, only one deal registration can be approved for a specific opportunity. As a reminder, deal registrations are opportunity based and partners cannot register an account.

Before approving or denying the Deal Registration the Channel Manager needs to check to see if an Opportunity already exists and either link or create one.

When a CSM receives an email about a deal registration to be managed, they should click the link to the reg and review the notes from the partner. 

It is best practice for a CSM to respond to a licensing deal registration first, and a managed service deal registration second. The process to approve/deny a managed service deal registration is the same as a resale deal registration. 

**To Approve a Deal Registration**

![Reg_Record_Steps](/handbook/sales/field-operations/channel-operations/images/Reg_Record_Steps.jpg)

Follow these steps _in order:_

![Link_Opportunity_Select](/handbook/sales/field-operations/channel-operations/images/Link_Opportunity_Select.jpg)

1. Click “Link/Create Opportunity.” 
    1. On the Link/Create Opportunity page, first search for the opportunity in the provided list and/or by doing a “Global Search.”
    2. **If the opportunity exists**, click “Select” next to the opportunity name. You will then be brought back to the deal registration record. 
    3. **If there is no matching opportunity**, click “Create New,” and then choose “Standard” in the Opportunity Type. The system will then populate all the necessary fields, including the DR-mapped fields. Click “Save” and you will be brought back to the registration record.
2. Click Approve/Deny/Return Registration.
3. On the next page, choose “Approve,” and if you have a message for the partner about the deal, you can add that into the “comments” section. This is not required, but anything included in this field will be sent back to the partner. 
 - If a distributor is involved, chose the distributor in the field that appears. If no distributor is involved, this field can be left blank.
4. Both the registration record and the opportunity record will be updated with the approval information.
5. Chatter @sales-support, @kim stagg, or @dennis zeissner and request that the opportunity owner be updated to the name of the sales team member who owns the customer account.

When a new customer account is created by "User Vartopia" during a deal registration, that customer account should be reassigned to the appropriate sales rep. For more information, [click here](https://about.gitlab.com/handbook/sales/field-operations/gtm-resources/#account-ownership-rules-of-engagement), or chatter @sales-support on the newly created customer account.

**To Deny or Request More Information on a Deal Registration**

1. Skip # 1 in the “Approve” process above, and click the “Approve/Deny/Return Registration.”
2. Choose “Deny” or “Return.”
    1. If you choose “Deny,” either choose the standard denial reason, or type out the reason for denial. This will be sent to the partner.
    2. If you choose “Return,” be sure to fill in the comments section with what the partner needs to do to complete a deal registration for approval. 

**Correcting Registrations Submitted Incorrectly**
If a partner submits a deal registration incorrectly (example: the wrong type of deal registration, deal, or incorrect information), GitLab Team Members cannot update this. At any point during the opportunity (until it is closed), the `DR - status` field can be changed to "returned." This will send an alert to the partner that there needs to be an update made. Please be sure to communicate with the partner and help them to provide the most accurate information.   

### Deal Registration System Status and Information
*   Deal Registration details will never override any information that the sales team forecasts on the Opportunity.
*   There is a Deal Registration ID that can be used to track all Deal Registrations in the system.
*   A Deal Registration is valid 90 days but can be extended if needed.
*   All Deal Registrations are individual objects in SFDC under Registrations. They do not come in as leads.

### Other Resources and Instructions
*   [Deal Registration Report](https://gitlab.my.salesforce.com/00O4M000004aUal)
*   [Channel Partner Handbook/Partner Program](https://about.gitlab.com/handbook/resellers)
*   [Deal Registration Fields and Definitions](https://drive.google.com/file/d/1pdPHZpR_0sOUJlat-USGvcXfFjumDwC9/view?usp=sharing)
*   [Adding the Registration Object to your Tabs](https://drive.google.com/file/d/1iUz42CfvYKPdw1quskc4NmraFVoZvFez/view?usp=sharing)
*   [Creating Personal Deal Registration Views](https://drive.google.com/file/d/1UtERcTNgNr9pTIf9OXDdpj3dNuhk9ISj/view?usp=sharing)
*   [Partner-Facing Step-by-step process](https://about.gitlab.com/handbook/resellers/#deal-registration-instructions)

Any questions or issues, please reach out to the #channel-programs-ops Slack channel. 


## Other Channel Opportunities

### Managed Service Provider (MSP) Opportunities

A Managed Service Provider (MSP) purchases licenses on behalf of an end user. The MSP will be the owner and manager of the licenses but their customer, the end user, is the one using the licenses. This creates specific needs in GitLab Salesforce opportunities to ensure proper reporting and compensation.

With an MSP opportunity, the Sales Reps need to follow these **_additional steps_** in the creation of the opportunity:

1. The opportunity must be created using the MSP partner account, NOT the potential customer on whose behalf they are purchasing.
2. Change the opportunity owner to the correct Sales Rep that owns the end-user account, even though the opportunity is created under the Partner MSP account.
3. Fill out the Partner and Deal Registration Information Section per the following:
    1. _DR-Partner:_ this must list the MSP’s _Partner_ account (same as the opportunity is created under)
    2. _DR-Deal type:_ "MSP"
    3. _DR-Engagement:_ Select applicable answer
        1. _If there is an approved Deal Registration, the Partner data will automatically be added when the Deal Registration is linked to the Opportunity. The DR-Engagement will be the only piece that needs to be filled out._
4. When filling out the quote for this opportunity, select the **MSP quote template**.
    4. Invoice owner: The MSP Partner (same as DR-Partner referenced above).
    5. Special Terms and Notes: Name of the End-user Customer (the MSP owns the licenses on behalf of).

### Service Attach Opportunities

A Service-Attached Deal Registration needs to be created to track when a partner offers their own professional services along with GitLab licenses. This is separate from the Deal Registration for the license sale.

To track the Partner Services, the partner must register the deal on the [partner portal](https://about.gitlab.com/handbook/resellers/#gitlab-partner-portal).

Please follow the steps above in the [Deal Registration Process](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#deal-registration-process)<span style="text-decoration:underline;"> </span>

Next, select the opportunity that is applicable to the GitLab sales of licenses that the services are being performed for. This GitLab sale opportunity may be open or closed won.

Rebate payouts will be reported and paid after each GitLab quarter close.

### Multiple-bid Process

For opportunities where there are multiple partners bidding on the same opportunity, it’s important that each partner gets the appropriate pricing for the opportunity.

*   The partner with the approved Deal Registration needs to receive the [documented discount for the program](https://gitlab.my.salesforce.com/0694M000008xAk4).
*   **All other partners quoting/bidding on the opportunity do not receive any partner discounts. They should be provided MSRP only.**
*   If the deal includes a distribution partner, that distributor receives their contracted margin.

For more information on quoting or the Partner Program, please visit:

*   [Deal Desk Quote Configuration](https://about.gitlab.com/handbook/sales/field-operations/sales-operations/deal-desk/#zuora-quote-configuration-guide---standard-quotes)
*   [Partner Program](https://about.gitlab.com/handbook/resellers/)
*   [Channel Discount Matrices for GitLab Team Members](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-program-discounts)


### Channel Tagging for PubSec FSI Deals - Temporary Exception Process 
_Effective Q2 FY22_

This process is specifically designed to recognize FSI Assist opportunities only, that eventually require fulfillment quotes to transact.

In the event an FSI provides Assistance on a deal (per the GitLab Partner Program definition of Assist) then the following tagging on the PubSec Opp can apply. An FSI is a Federal System Integrator and can be identified as part of their Account Record in SFDC.

 - DR - Partner = Reseller Partner Account Transacting the Opportunity through Carahsoft Technology Corporation 
 - DR - Partner Deal Type = Resale
 - DR - Partner Engagement = Assist
 - Distributor = [Carahsoft Technology Corporation](https://gitlab.my.salesforce.com/0014M00001ilpft)
 - Fulfillment Partner = Platform Partner = FSI Partner Account
 - Channel Manager = Account Owner of the FSI Partner Account

![PubSec_TempProcess](/handbook/sales/field-operations/channel-operations/images/PubSec_TempProcess.jpg)

The expectation is that the Quote will receive the Partner Program “Fulfilment” discount instead of the standard Assist discount. Since the Opportunity is tagged as Assist, this will trigger Channel Approvals on these quotes. The following comments should be added to the note section on the approval by the PubSec Channel Director:

_“Discount exception approved due to interim FSI process”_

Since PubSec does not qualify for Comp Neutral, comp neutral calculations will not be impacted.

Expectation is that the FSI account owner will receive quota retirement and comp on these Opportunities against their Assist component of their plan. Regional bookings will apply to the transacting Partner Account Owner/rest of the PubSec Channel team. 

For any questions, please reach out to #pub-sec-channels.



## SFDC Opportunity Source Field Values for Channel

**SFDC Opportunity Fields**:

-  _Initial Source:_
   - **Channel Qualified Lead (CQL):** GitLab Channel Partner created and/or qualified the Lead whether they sourced it themselves or GitLab provided the inquiry to them to work.
-  _Sales Qualified Source:_
   - **Channel:** Channel Partner has converted the Lead/CQL to a Qualified Opportunity. This field defaults to Channel when Initial Source = _CQL_.
- _DR - Deal Engagement:_
   - **Partner Sourced:** Partner has either found the original opportunity or it is an upsell to a current customer. If the `Initial Source = Channel Qualified Lead` or `Sales Qualified Source = Channel Generated`, then the deal is Partner Sourced.
   - **Assist:** Partner Assisted Opportunity.
   - **Fulfillment:** Partner Fulfillment Opportunity.

*For additional definition and qualification of Deal Engagement type go [here.](https://about.gitlab.com/handbook/sales/field-operations/channel-operations/#partner-engagement-types

**For additional Source definition please visit the [Marketing Handbook Page. ](https://about.gitlab.com/handbook/marketing/marketing-operations/#initial-source)


## Partner Help Desk Support and Communication

**Internal Team Members:** Chatter `@Partner Help Desk` in Salesforce or for general questions, post in the Slack channel #channel-programs-ops.

**External Communication:** Email partnersupport@gitlab.com to include a partner or other external stakeholder for help with partner-related requests.

PHD team members monitor the queue and email inbox throughout the day in all time zones. 

Partner Help Desk’s primary internal communication channel is Salesforce Chatter. When you chatter `@Partner Help Desk`, it will automatically create a case in the Partner Help Desk (PHD) queue. **Please do not tag PHD team members directly in chatter or make a request through Slack direct message. Always use `@Partner Help Desk` for SFDC requests or post in #channel-programs-ops in Slack for general questions.** 

This ensures our team is working as efficiently as possible for everyone and that you are covered in case the team member who replied first becomes unavailable. 

If someone is working on a case, they will continue to support until the case is closed. If a matter was resolved, but needs to be revisited, please chatter `@Partner Help Desk` to reopen a case.

To Chatter the PHD team, tag `@Partner Help Desk` in Chatter on the related opportunity or account page and a short sentence on your request. If the PHD team needs more information or needs to pull in another team, we will follow up directly via Chatter.

If you need to involve a partner, please email partnersupport@gitlab.com, instead of an individual PHD team member so any team member can jump in as something moves forward.



##  Program and Incentive Definitions

### Partner Program Discounts
- The GitLab Partner Program provides partners with set discounts based on their engagement in opportunities (see definitions below) and their partner program track.
- At least one partner employee must complete the Sales Core training for the partner to qualify for deal registration and program discounts.
- GitLab employees can access the discount tables using the links below:
   -  [Commercial Matrix](https://gitlab.my.salesforce.com/sfc/p/61000000JNOF/a/4M000000gO1K/ZtACDN.hFA1h_ST3qrbWSnzw6HQosEw467XKt6XHN2s)
   - [Public Sector Matrix](https://gitlab.my.salesforce.com/sfc/p/61000000JNOF/a/4M000000gO1F/UqIsQERnDC.vjdlNcxG9CmzaRbewWFtuh5IIhkEx_ng)
   - [MSP Matrix](https://gitlab.my.salesforce.com/sfc/p/61000000JNOF/a/4M000000gO1A/2WB5w5FkEJFBN_jcjmA9.9fp8n5rxvkOYpr9.mQ_1JM)
 
Partners can find the discount table in the Asset Library on the [GitLab Partner Portal](partners.gitlab.com).




### Partner Engagement Types
**Partner Sourced**



*   A Partner Sourced opportunity is an opportunity where a Partner has either found an original opportunity or is upselling a current customer.
*   An opportunity can only be Partner Sourced if the Initial Source = Channel Qualified Lead or Sales Qualified Source = Channel Generated.
*   A Partner Sourced opportunity could be:
    *   An opportunity with new customer to GitLab.
    *   An opportunity with a customer/prospect with which GitLab is already engaged, but was not aware of the specific Partner Sourced opportunity.
    *   An existing customer upgrade to a higher pricing plan. This could be for a customer that was originally sold by GitLab.
    *   Additional licenses sold, often at the time of renewal. This can also be an increase in size of an existing GitLab sales opportunity.
    *   True-ups to an original partner opportunity.
*   The opportunity must be new to our sales team or added to an existing sales opportunity, and can be for a new or existing customer.
*   The partner is expected to collaborate with the GitLab Sales team to jointly close the sale.
*   For US Public Sector, each unique customer opportunity within a single government program can be Partner Sourced.
*   For resale, the partner receives an upfront discount that is dependent on the partners track within the GitLab Partner Program.
*   Referral rebate payments are paid out no later than 45 after the end of each GitLab fiscal quarter.
*   The determination of Partner Sourced is per the system logic stated above and tracked via Salesforce opportunities.


**Partner Assist Opportunity**



*   Any opportunity where the partner assists our sales team in closing the deal.
*   This assistance may include any or all of the following: a customer demo/POV, an executive introduction meeting, delivery of services, helping with the transaction, financing. Often this is leveraging the partner's incumbency.
*   Partners need to submit a Deal Registration for Partner Assist. Since it is for a GitLab sourced opportunity, it does not qualify to be Partner Sourced, but should be tagged as Partner Assist in Salesforce.
*   The determination of Partner Assist is at the sales rep & CAM determination and tracked via SFDC opportunities.
*   Partner must take paper/transact the deal in order for it to qualify for Assist. 


**Partner Fulfill Opportunity**



*   Any opportunity that was fulfilled by a partner but closed independently via the GitLab sales team.
*   The partner has only processed the order and didn’t provide any meaningful support to close the deal.


**Services Attach Opportunity**



*   The resale discount will be administered as an upfront discount from the GitLab license price on the most recent product sale net license price.
*   Partner Service Attach incentives are as follows: 
    *   2-4 Services per quarter: 2.5% on license net ARR (Max payout $2500/deal)
    *   5-9 Services per quarter: 5% on license net ARR (Max payout $5000/deal)
    *   10 or more Services per quarter: 7.5% on license net ARR (Max payout $7500/deal)
*   Partners must register a Services Attach deal registration and provide proof of performance to qualify for the incentive.
*   Rebate payments are paid out at the end of each GitLab fiscal quarter.
*   Rebates and referral fees may require CRO approval.


**Services Resale**



*   Partners can earn a program discount for reselling GitLab Professional Services delivered services.
*   To qualify for the Services discount, the services must be included on the order of a deal registered opportunity.



**Incumbency Renewals**



*   Incumbent partners qualify for program renewal discounts. The partners that transacts the most recent sale  are considered the incumbent
*   A different partner can earn an incumbency discount only through formal written communications from the customer. This can be provided via email from an authorized representative from the customer.
*   In some cases, a customer purchased their most recent subscription directly from GitLab, but is directed to a partner for renewal. Partners are encouraged to engage with these customers, but their first renewal of a formerly direct customer will not be discounted for partners.
*   To earn partner discounts, partners will be required to meet compliance requirements (i.e. in good credit standing, have provided quarterly updates on customer, review within 30 days of renewal, etc).


**Tender Offers**



*   Tender offers are ones where the customers are requesting multiple bids for a project.
*   Each partner bidding should register the deal. Since all partners would be engaged in the sales process and would be creating a bid, all partners qualify for Partner Assist discount. If the first partner registering the deal was early in with the customer (pre-tender), introduces the opportunity to GitLab, and the appropriate system logic fits, that partner could earn a Partner Sourced discount. If the partner earning the Partner Sourced discount is not awarded the deal, they would not receive additional referral compensation. Any partner offering services would qualify for Services-Attach incentives in addition to any sales discounts for which they would qualify.


**Program Compliance**



*   For partners to transact according to program discounts, they must agree to the GitLab Partner Agreement.
*   To earn partner discounts, partners will be required to be program compliant.
*   Non Contracted partners may transact on a one-off basis, only with approval of channel leadership.


**Unauthorized Partners**



*   Unauthorized partners are ones that have not signed a GitLab Partner Agreement.
*   Unauthorized partners cannot transact GitLab products and/or services, unless rare but explicit approval is granted. Please reach out to the #channel-programs-ops Slack channel.
*   If an unauthorized partner would like to transact GitLab products or services, please have them visit the [Partner Portal ](partners.gitlab.com) to sign up. The process is fairly simple and quick.
*   A key goal of the GitLab Channel Program is the success of our authorized partners. We are developing our channel to provide coverage across geos, customer segments and vertical markets.



## Partner Applicant Approval / Denial - Granting Portal Access

Partner Program participation sign ups must be initiated by the Partner in the Partner Portal application form which can be found [here.](https://rv.treehousei.com/en/login.aspx)  In the partner application process, channel partners review the partner contract, including both the resale and referral addenda, review the partner program guide, complete their application form and agree to program terms and conditions.   Technology partners are not able to agree to the terms and conditions during the application process.

 If an authorized representative of the channel partner accepts the agreement terms, they (or the person entering the application) will need to select “Yes” that they agree to terms on the application form.  Once they have agreed, they will automatically be set to “ Authorized” and will get immediate access to the partner portal.  At this time, partners will be set up in both Salesforce and the partner portal at Authorized and their track set to Open.

The partner will receive an email confirming the receipt of their application, and applicable Channel Sales or Alliance Manager will receive a New Partner notification email from Partnersupport@gitlab.com notifying there is a new partner applicant in that region.  Channel Sales Managers will be notified of each partner application in their regions, whether they agreed to the terms or not.

Upon receiving notification they will be responsible for reviewing the partner’s information and deactivating any inappropriate partners.  They will also need to set the Partner Type in Salesforce for newly authorized partners.

For partners that have questions about the contract or need to negotiate terms and conditions, Channel Sales Managers are responsible for working with the partner offline to address questions and come to agreement on program terms.  Upon receiving the New Partner Applicant notification email, the applicable Channels Sales Manager needs to complete the following:

1. Contact the partner and qualify them.
2. If the decision is to move forward with the partner first check to see if a partner account already exists in Salesforce. If it is a duplicate, request for the accounts to be merged by the Channel Operations team. If the decision is to deny the partner then go to step #7.
3. To start the contracting process click the Legal Request button in SFDC on the partner account record.
    *   Request the appropriate contract addendum (Resale, Referral/Services or both **OR** MSP **OR** OTHER). Default should be Resale and Referral/Services.
4. Once the contract is fully executed and attached to the partner account record in SFDC the following fields need to be updated by the Channel Sales Manager and are required(*) in order to save the account.
    *   *Change Partner Status = Authorized.
    *   *Select Partner Type.
    *   For partners that signed standard contract terms, set Partner Program Status to “New”.
    *   Please update the partner record to be as complete as possible.
    *   For additional information on the Partner Program review [here](https://about.gitlab.com/handbook/resellers/#partner-program-tracks)
5. Once a partner is authorized, each SFDC contact for that partner will automatically receive a message with login credentials to the portal.
6. Additional partner employees can go to partners.gitlab.com to register. \ Once they are linked to an authorized partner account (they must select the correct account upon registering), they will automatically receive a message with login credentials. If the account is still a Prospect they will not have access until the account has an executed contract and is moved to Authorized.
7. If the decision is to not move forward with the partner, the Channel Sales Manager needs to set Partner Status = Denied.

Technology partners use the same form, but are not able to agree to the terms and conditions.  Once they submit the form, they will be set to active.  If the Alliances team wants to establish a contract with the partner, they must follow the Legal Request process in Salesforce.

If for any reason, a partner account needs to be created in Salesforce directly, requests for account creation can be made to #channel-ops within Slack.

Visit the [Partner Applicant / Partner Portal FAQ](https://docs.google.com/document/d/1aPCqF5-qb2XxFEhvkNzvexwsIYGuiJF8AhK_qeUgw0Y/edit?usp=sharing) for additional information.



## Channel Partner Price Files
GitLab will provide Channel Price Files for distributors and direct resellers approximately 30 days before intended changes. Each price file will have three formats: Google Sheet, Excel Spreadsheet, and PDF. **Only Channel Managers should be sharing Channel Price Files.**

The following price files are provided by Channel Ops:



*   Distribution Price Files for Resale Opportunities, including reseller and distributor discounts for the main program.
*   Public Sector Price Files for Resale Opportunities, including reseller and distributor discounts for the main program.
*   Partner (Direct Reseller) Price Files for Resale Opportunities, including reseller discounts for the main program.
*   List Price File with no discounts.

### Locating and Sharing Channel Price Files

Price Files can be found [in this folder](https://drive.google.com/drive/folders/1UCNH77wTF4eCiCeAQHqDItkCGITl534D?usp=sharing)).

When sharing a Channel Price File with a partner (either a distributor or reseller), please do NOT share the folder or file location. To share a document, please either copy it into your own google drive and update the permissions accordingly when you share a link, or attach a downloaded copy to an email to a partner. No partners should be given access to this folder. Only Channel Managers should be sharing Channel Price Files. 

### Naming Conventions and Which File to Use

Within the Price List Folder, there are other folders. For the current active price file, always use the one with the most recent date that has not passed yet. The folder name will also say [ACTIVE] at the front of it. 

For example: 

If there are three folders within the Price List Folder. One is dated two months ago, and one is dated for one month in the future. The one dated two months ago says [ACTIVE] in front of the file name.  \
The file dated two months ago is the one currently in use with our partners. For current questions and quoting purposes, this is the file that should be used. 

The file dated one month in the future is the file that should be provided to partners (especially distributors) to set up their systems. It will go into effect on the date in the file name. 

If there are any questions, please reach out to the #channel-programs-ops Slack channel.



## Alliances and OEMs

[Alliances Salesforce Dashboard](https://gitlab.my.salesforce.com/01Z4M000000oYAp)

_[Internal Only Alliance Handbook](https://gitlab-com.gitlab.io/alliances/alliances-internal/)_

For any questions regarding our Alliance partners, please reach out to the #alliances Slack channel.

### Opportunity Tagging for Google Cloud and Amazon Web Services Deals

If a deal is being transacted through **GCP Marketplace** or **AWS Marketplace**, then the following fields needs to be filled out on the Opportunity:



*   **DR - Partner** should be filled out using these SFDC accounts:
    *   [https://gitlab.my.salesforce.com/0014M00001nJhks](https://gitlab.my.salesforce.com/0014M00001nJhks)
    *   [https://gitlab.my.salesforce.com/0014M00001ldTdt](https://gitlab.my.salesforce.com/0014M00001ldTdt)
*   **DR - Partner Deal Type** = Resale
*   **DR - Partner Deal Type** = Partner Sourced, Assist, or Fulfillment

If Google or AWS has brought us a lead/referred us a deal but will not be transacting on their Marketplace, then the following fields should be filled out on the Opportunity:



*   **DR - Partner** should be filled out using these SFDC accounts:
    *   [https://gitlab.my.salesforce.com/0014M00001nJhks](https://gitlab.my.salesforce.com/0014M00001nJhks)
    *   [https://gitlab.my.salesforce.com/0014M00001ldTdt](https://gitlab.my.salesforce.com/0014M00001ldTdt)
*   **DR - Partner Deal Type** = Referral
*   **DR - Partner Deal Type** = Partner Sourced

If Google or AWS has assisted on a deal and helped drive the customer to buy GitLab, but was not the original source of the opportunity, then the following fields should be filled out on the Opportunity:



*   **Influence Partner** = should be filled out using these SFDC accounts:
    *   [https://gitlab.my.salesforce.com/0014M00001nJhks](https://gitlab.my.salesforce.com/0014M00001nJhks)
    *   [https://gitlab.my.salesforce.com/0014M00001ldTdt](https://gitlab.my.salesforce.com/0014M00001ldTdt)

### Requesting Google Cloud Credits

Required fields when requesting Google Cloud Credits on an Opportunity



1. Have you engaged with the GCP Team already? (Drop down: Yes, No)
2. Customer open to being a reference? (drop down: logo use, case-study, joint speaking session, etc.)
3. Credits being requested (Sales Rep enters in the amount of credits required to close the deal)

Once all required information is provided, it will be routed internally for approval. For more information, visit the [Private Alliance Handbook Page](https://gitlab-com.gitlab.io/alliances/alliances-internal/gcp/).



## Compensation on Channel Opportunities 
### Channel Neutral

Comp Neutrality applies to all GitLab Opportunities where a Partner is involved in the physical  transaction (takes paper) **<span style="text-decoration:underline;">and</span>** a Partner Program discount is applied on the executed Partner Order Form.

The maximum Comp Neutral payout is based on the applicable GitLab Partner Program discount matrix. If additional discount is needed in order to close the deal, that portion is not eligible for Comp Neutrality.

In the event a lesser discount is applied on the deal then what the Partner Program allocates, then the lesser discount will also be what is calculated for Comp Neutrality.

As a reminder, comp neutrality only applies to comp and does not retire quota. For additional information, please review [Channel Neutral Compensation](https://about.gitlab.com/handbook/sales/commissions/#channel-neutral-compensation) within the Sales Ops commission section of the Handbook.

Internal: [Partner Program Discount Matrix](https://gitlab.my.salesforce.com/sfc/p/61000000JNOF/a/4M000000g8Ba/fMhmeefW.jeiHK3_Hq5iANC3ogdHk9N3j0MgdWIpmI4)

For Partner Program discounts for PubSec, please reach out to #channel-ops on Slack.

### Comp Neutral Calculation in SFDC

In the event the opportunity is going thru a Partner and therefore qualifies for CN, then the appropriate partner information must be filled out on the Opportunity and Quote in order for the CN field to calculate properly.

Required Partner Fields on the Opp:
*   DR - Partner
*   DR - Partner Deal Type
*   DR - Partner Engagement
*   Distributor _(as applicable)_

Required Partner Info on the Quote
*   Invoice owner contact and Invoice owner Type must be a Partner Account and Contact
*   Order Form must be a Partner Order Form

**Example Calculation No. 1:**

```
Partner Track               : Select
Product                     : Standard (Previously Premium)
QTY                         : 25
Term                        : 1 year
Partner Deal Type           : Resale
Partner Engagement          : Partner Sourced
Deal Registration           : Approved
Partner Program Discount    : 15%
```

- $228 x 25 x 1 = $5700 (price without program discount aka MSRP or List Price)
- $228 x 25 x 1 x 15% = $4845 (quote/order form price with discount)
- $5700 x 15% = $855 (comp neutral amount)


**Example Calculation No. 2 (PubSec):**

```
Partner Track               : Open
Distributor                 : True
Product                     : Ultimate
QTY                         : 50
Term                        : 1 year
Partner Deal Type           : Resale
Partner Engagement          : Assist
Deal Registration           : Approved
Partner Program Discount    : 10%
```

- $1188 x 50 x 1 = $59400 (price without program discount)
- $1188 x 50 x 1 x 15% = $50490 (quote/order form price with discount)
- $59400 x 15% = $8910 (comp neutral amount)

### 2-Tier Opportunities

If the Opportunity is going through a 2-Tier Channel (Distributor + Reseller), both Partner Accounts must be included on the Opportunity in the Partner Information section in order for both Program discounts to apply AND for Comp Neutral to calculate on both. Reseller should be in the DR - Partner field and the applicable Distributor in the Distributor field on the Opportunity.

### Opportunities with GCP and AWS

For deals going through GCP and AWS, the Partner fields should still be filled out on the Opportunity but comp neutral will not calculate until the deal closes as partner discounts are done after the quote/order form is generated. Deal Desk will assist with this process.



