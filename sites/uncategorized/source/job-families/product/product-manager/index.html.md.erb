---
layout: job_family_page
title: "Product Manager"
description: "Product Managers at GitLab have a unique opportunity to define the future of the
entire DevOps lifecycle" 
---

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/DVLOyaRbAoM" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

## Role

Product Managers at GitLab have a unique opportunity to define the future of the
entire [DevOps lifecycle](https://about.gitlab.com/stages-devops-lifecycle/). We
are working on a [single application](/handbook/product/single-application) that
allows developers to invent, create, and deploy modern applications.

We want to facilitate [Concurrent DevOps](https://about.gitlab.com/concurrent-devops/) so that teams can work simultaneously instead of sequentially, unleashing collaboration across organizations.

We work in a very unique way at GitLab, where flexibility and independence meet
a high paced, pragmatic way of working. And everything we do is [in the open](https://about.gitlab.com/handbook/).

We are looking for talented product managers that are excited by the idea to
contribute to our vision. We know there are a million things we can and want to
implement in GitLab. Be the one making decisions.

We recommend looking at our [about page](/company/) and at the [product handbook](https://about.gitlab.com/handbook/product/)
to get started.

<%= partial 'includes/pm_responsibilities' %>
<!-- update in /source/includes/_pm_responsibilities.erb -->

<a id="base-pm-requirements"></a>
### Base Requirements Across All Levels

- Experience in product management
- Strong technically: you understand how software is built, packaged, deployed and operated
- Passion for design and usability
- Highly independent and pragmatic
- Excellent proficiency in English
- You are living wherever you want and are excited about the [all remote](https://about.gitlab.com/company/culture/all-remote/) lifestyle
- You share our [values](/handbook/values/), and work in accordance with those values
- Have a growth mindset
- Trust and empower your development group to deliver great products
- Receptive to feedback or coaching
- Strong bias for action
- You make thoughtful decisions motivated by data and research
- You develop strong opinions but are willing to change your mind
- Bonus points: Experience with GitLab, Git, and the Git workflows
- Bonus points: Experience building DevOps tools
- Bonus points: Experience in working with open source projects
- Bonus points: Experience engaging with customers in a B2B setting

### Primary Performance Indicator for the Role

[Stage Monthly Active Users](https://about.gitlab.com/handbook/product/metrics/#adoption)

***

## Career paths

### In role levels

Read more about [levels](/handbook/hiring/vacancies/#definitions) at GitLab here. Within the Product Management Career Track we use the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/) to determine requirements for PM roles.

#### Intermediate Product Manager

Beyond the [base requirements](#base-pm-requirements), Product Manager requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/).

##### Job Grade

The Product Manager is a [grade 8](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

#### Senior Product Manager

Senior Product Managers are expected to be experts in their product domain and viewed as such to the community and internally at GitLab. They are expected to prioritize and manage their products with minimal guidance from leadership or other product team members.

##### Job Grade

The Senior Product Manager is a [grade 9](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Expectations

Senior Product Managers are leaders at GitLab. In addition to doing the [core of the PM](/handbook/product/product-manager-responsibilities/) job well, we expect them to do the following:

**Drive the product in the right direction**
* Consistently deliver outsized impact to their stage and category KPIs or other [GitLab KPIs](https://about.gitlab.com/company/kpis/#gitlab-kpis)

**Take an active role in defining the future**
* Mastery of the competitive and market landscape of their product domain and understanding how this landscape impacts the product roadmap
* Innovate within your product area by proposing ambitious features

**Manage the product lifecycle end-to-end**
* Document ROI or impact for a given action, feature, or prioritization decision
* Execute to deliver outsized results on the aforementioned ROI/impact analysis

**Engage with stakeholders in two-way communication**
* Represent GitLab as a product and domain expert in front of industry analysts, strategic customers, industry events/conferences, and other events
* Ability to present to C-level executives both internally at GitLab and externally to customers and prospects

**Lead by example**
* Mentor less experienced Product Managers to enable them add more value sooner

##### Requirements

Beyond the [base requirements](#base-pm-requirements), Senior Product Manager requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/).

#### Principal Product Manager

The Principal Product Manager role extends the Senior Product Manager role expectations.

##### Job Grade

The Principal Product Manager is a [grade 9.5](/handbook/total-rewards/compensation/compensation-calculator/#gitlab-job-grades).

##### Expectations

**Bring special expertise in a specific domain or Product Management skill**
* Principal Product Managers should possess a unique level of expertise as well as own specific domains within the product that extends cross-stage. This includes providing guidance on improvements within this specific domain with a focus on usability and user experience.

**Ability to coach others**
* Principal Product Managers are expected to coach and mentor other Product Managers as well as assist with their career development. This includes being a “PM buddy” with regular check-ins on larger initiatives with a focus on elevating the Product Manager's [Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/) track skills.

##### Requirements

Beyond the [base requirements](#base-pm-requirements), Principal Product Manager requirements are outlined in the [Product Management Career Development Framework](https://about.gitlab.com/handbook/product/product-manager-role/product-CDF-competencies/).

### Moving to and moving from

The career paths to and from product management positions are varied, but there are some common patterns.

#### Moving to Product Management

Successful product managers have a passion for solving problems. As a result they've
been found to start their journey to product management from the following disciplines:

* **Engineering**: Engineers understand deeply how the product is built and how they work. A desire to understand the user problem of their product 
and to further influence product direction can lead engineers to pursue Product Management roles.
* **UX**: Product Designers and UX leaders have built a great understanding of the personas and pain points their product
targets. A desire to further influence not just the experience but the way the features alleviate those
pain points can draw UX professionals to Product Management.
* **Engineering Manager**: Engineering team leaders who've gained a strong understanding of the customer persona, pain
points and direction of a product often want to further influence that direction by transitioning to Product Management roles.
* **QA/QE**: Quality engineers are responsible for the validation of a product and service. Those who
have a passion for understanding and testing how products are used in the real world by real users often
find a strong alignment to the Product Management function.
* **Product Marketing**: Product Marketers with a desire to have more influence in shaping the product often
migrate to the Product Management function.
* **Customer Success**: Customer facing functions like Solutions Architects, Technical Account Managers, and
lead Support techs who've developed a strong understanding of customers' needs in the product often transition
to Product Management out of a desire to shape the direction of the product.

#### Moving from Product Management

Within their role, Product Managers are empowered to interact and learn more about functions they are interested in.
Whether that be Marketing, Customer Success, Support, Finance or Engineering - Product Managers are encouraged (and often
required) to understand other functions as part of their daily responsibilities. As such there is plenty of room for
Product Managers to transition to roles outside of the Product Management team. Some of those include:
* **Engineering Leadership**: Product Managers often find a desire to dive deeper into Engineering problems, or grow
mentor and lead engineering teams.
* **Product Marketing**: Product Managers often gravitate to how we target, message and deliver value to our customers.
Product Marketing roles are an excellent way to further that expertise.
* **Customer Success**: Few people know our products as well as Product Managers, and product managers who enjoy directly
solving customers problems on a daily basis make great fits for Customer Success roles.
* **General Management**: As a result of their exposure to a wide variety of functions, Product Managers often make a
transition from Product Management to General Management.

***

## Specialties

### Manage (Access)

We're looking for product managers that can help us work on the future of developer tools. Specifically, building out our lovable support for GitLab by driving strategy to help organizations prosper with access and authentication functionality that enables them to work more efficiently.

#### Requirements 

- Knowledge of the Authentication and Authorization space, such as: SAML, LDAP, OAuth, IdAAS, SSO, SCIM

<figure class="video_container">
  <iframe src="https://www.youtube.com/embed/GhUAv1XlhVU" frameborder="0" allowfullscreen="true"> </iframe>
</figure>

### Create

We're looking for product managers that can help us work on the future of developer tools. Specifically, building out our lovable support for GitLab by driving strategy around git, source code management, code review, and the developer experience.

#### Requirements
- Strong understanding of Git and Git workflows
- Knowledge of the developer tool space and workflows
- Practical understanding of source code management tools such as GitHub, Phabricator, or Gerrit

### Verify (CI)

We're looking for product managers that can help us work on the future of DevOps tools; specifically, building out continuous integration (CI), code quality analysis, micro-service testing, usability testing, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Practical understanding of container technologies including Docker and Kubernetes

### Release (CD)

We're looking for product managers that can help us work on the future of the [Release stage](https://about.gitlab.com/direction/release/) and [deployment](https://about.gitlab.com/direction/deployment/) capabilities at GitLab. The PM will work on building out continuous delivery (CD), release orchestration, features flags, and more.

#### Requirements

- Strong understanding of DevOps, CI/CD, and Release Automation
- Understnad the challenges of complex, enterprise-level deployment
- Practical understanding of container technologies including Docker and Kubernetes
- Practical understanding of progressive delivery and advanced deployment techniques

### Configure

We're looking for product managers to help us work on the future of DevOps tools; specifically, building out configuration management and other operations-related features.

#### Requirements

- Strong understanding of CI/CD, configuration management, and operations
- Understanding of deployment infrastructure and container technologies
- Practical understanding of container technologies including Docker and Kubernetes

### Package

We're looking for product managers to cover the [Package stage](/handbook/product/categories/#package) of our DevOps lifycle. This candidate will work specifically on building out packaging categories and features such as Docker container registry and binary artifact management.

#### Requirements

- Strong understanding of CI/CD and package management
- Understanding of the complexity of managing multi-artifact application deployment
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Monitor

We're looking for product managers to build the future of [Monitoring stage](https://about.gitlab.com/direction/monitor/). This PM will work to build out the Monitor categories and features, such as Incident Management, Synthetics, and observability tools.

#### Requirements

- Strong understanding of the SRE persona and their workflow
- Practical understanding of observability

### Distribution

We're looking for product managers to support our [Distribution group](/handbook/product/categories/#admin) and manage our installation, upgrade, and configuration process for our self-managed customers.

#### Requirements

- Strong understanding of system administration
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Secure

We're looking for product managers that can help us work on the future of developer tools; specifically, building out application security testing, including static analysis and dynamic testing.

#### Requirements

- Strong understanding of CI/CD and automated security testing
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Protect

- Strong understanding of SIEM and operational security
- Practical understanding of deployment infrastructure and container technologies including Docker and Kubernetes

### Product Intelligence

We're looking for product managers that can help us mature our product usage data set to enable better decision making across the company.

#### Requirements

- Strong analytical skills, including an ability to translate business needs back to underlying data sets and structures
- Previous experience with data and analytics technologies
- Excellent communication skills at all levels, including e-group

### Growth

We're looking for Product Managers to help us grow our customer base by improving our net new signup flow (Acquisition), trial flow (Conversion), upgrades & seat adds (Expansion), and renewals (Retention).

#### Requirements

- Well versed in experimentation and A/B testing
- Strong analytical skills and a hypothesis driven mindset
- Previous experience leveraging common frameworks and growth techniques

### Fulfillment

We're looking for a PM to ensure an excellent purchase, trial, upgrade, seat addition, and renewal experience for GitLab customers.  This role will involve creating a flexible and powerful billing and licensing system upon which we can build world class customer experiences.  The role will also involve important system level integrations with key 3rd party systems such as Zuora and Stripe.

#### Requirements

- Strong business operations skills and an ability to think systematically about complex workflows and work cross-functionally to ensure an excellent end-to-end customer experience
- Previous experience with high scale billing & licensing systems
- Familiarity with commercial best practices for no touch, sales assisted, and partner assisted transactions
- Excellent communication skills at all levels, including e-group

### Infrastructure

We're looking for a product manager to work closely with our Infrastructure teams, which own the operational aspects of running GitLab.com as well as other services.

#### Requirements

- Deep understanding of the full DevOps lifecycle and tools, with actual experience working in a DevOps role a plus
- Practical understanding of what it takes to run internet applications at scale

#### Responsibilities

- Own prioritization, validation, and scoping of strategic projects and initiatives for the Infrastructure teams
- Champion dogfooding of the GitLab product wherever possible; if the efficacy of dogfooding is in doubt, drive a thoroughly documented build/buy analysis
- Champion cross-product outcomes like fast response times and low operational costs on behalf of the Infrastructure teams
- Ensure the Infrastructure teams are tightly connected in with the mainline Product and Engineering teams

### Search

We're looking for a product manager to make finding things easy and delightful in GitLab. Your goals are to improve the global search experience within GitLab, and ensure the solution can scale from small self-managed instances to GitLab.com.

#### Requirements

- Strong understanding of search engines, algorithms, and workflows. Direct Elasticsearch experience is a plus
- Familiarity with Search workflows, and helping users find what they are looking for
- Understanding of the DevOps lifecycle and tools a plus

### Memory & Database

We're looking for a PM to drive improvements to the responsiveness and scalability of GitLab, and establish best practices for performance oriented development. This role will involve proactively identifying and prioritizing performance hotspots, leading needed architectural changes for additional scalability and efficiency, and delivering improvements to our development process for writing performant code.

#### Requirements

- An understanding of how complex applications function, and an ability to think systematically about application flows
- Strong analytical skills, and a quantitative approach to incremental performance improvements
- Ability to translate best practices into product changes for GitLab
- Hands-on experience with databases, caching strategies, and web applications

## Relevant links

- [Product Handbook](/handbook/product)
- [Engineering Workflow](/handbook/engineering/workflow)
