---
layout: markdown_page
title: "Category Direction - Templates"
description: "Project templates make it easy to setup a new project by starting from an existing one that already has all the required configuration, files, and boilerplate."
canonical_path: "/direction/manage/templates/"
---

- TOC
{:toc}

Last Reviewed: 2021-05-17

## Introduction

Thanks for visiting the direction page for Templates in GitLab. This page belongs to the [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group of the [Manage](https://about.gitlab.com/direction/manage/) stage and is maintained by [Haris Delalić](https://gitlab.com/hdelalic) who can be contacted directly via [email](mailto:hdelalic@gitlab.com). This vision is a work in progress and everyone can contribute. If you'd like to provide feedback or contribute to this vision, please add them as comments in the corresponding [epic](https://gitlab.com/groups/gitlab-org/-/epics/2767) for this category.

## Mission

Project templates make it easy to setup a new project by starting from an existing one that already has all the required configuration, files, and boilerplate. GitLab provides a variety of templates as a starting point for creating new projects and it's also possible to contribute to these; administrators can even configure templates specific to your GitLab instance.

## Overview

We already have dozens of templates for common implementations: [Project templates](https://gitlab.com/gitlab-org/project-templates), [CI YAML templates](https://gitlab.com/gitlab-org/gitlab/tree/master/lib/gitlab/ci/templates), [Pages templates](https://gitlab.com/pages), and even [example projects](https://gitlab.com/gitlab-examples). Please note that only the [Project templates](https://gitlab.com/gitlab-org/project-templates) functionality is a focus for the [Import](https://about.gitlab.com/handbook/product/categories/#import-group) group.

- [Issue List](https://gitlab.com/groups/gitlab-org/-/issues?scope=all&utf8=%E2%9C%93&state=opened&label_name%5B%5D=Category%3ATemplates)
- Documentation: [Project templates](https://docs.gitlab.com/ee/user/project/working_with_projects.html#project-templates), [Custom group-level project templates](https://docs.gitlab.com/ee/user/group/custom_project_templates), [CI YAML templates](https://docs.gitlab.com/ee/ci/examples/README.html#cicd-examples), [Enterprise templates](https://docs.gitlab.com/ee/user/project/working_with_projects.html#enterprise-templates)

## What's next & why

The Import group is currently only working on critical bugs and security fixes for the existing Templates functionality.

In 2021, we will attempt to make progress toward [refining the Template Contribution Process](https://gitlab.com/groups/gitlab-org/-/epics/863). The current process for creating additional templates involves manual steps. We are looking to automate those steps to make this process easier for everyone to contribute new templates.

## What is not planned right now

Adding new templates will not be prioritized by this group. However, we encourage anyone to contribute new templates to GitLab and we will actively support those efforts.

This feature has been discussed, but there are currently no plans to work on it:

- [Parameterize Project Templates](https://gitlab.com/gitlab-org/gitlab/issues/26580). A top customer issue is a request to parameterize project templates to enable templates configured with variables to prompt the user for inputs that will eliminate manual customizations after the project is created. We will reevaluate the priority of this feature in the future.

## How you can help

Don't see a template that you wish we had? Please consider [contributing](https://about.gitlab.com/community/contribute/project-templates/) that template to GitLab. 
